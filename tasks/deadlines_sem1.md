***Note*** Время дедлайна всегда 23:59

   TaskName  | Deadline | Points | WithReview | Required
-------------|----------|--------|------------|---------
  sum_of_two |30.10.2022|    1   |     No     |   True
factorization|30.10.2022|    2   |     No     |   False
  2d-spiral  |30.10.2022|    3   |     Yes    |   False
  bin search |06.11.2022|    3   |     Yes    |   False
 ring buffer |06.11.2022|    4   |     Yes    |   False
 string      |12.11.2022|    6   |     Yes    |   True
 BigInteger  |20.11.2022|    6   |     Yes    |   *
  Geometry   |18.12.2022|    8   |     Yes    |   *
  Matrix     |18.12.2022|    8   |     Yes    |   *

*Так же нужно обязательно сдать одну из трех задач: BigInteger/Geometry/Martix (то есть required это сдача 1/3)

То есть в сумме обязательных задач 3: sum_of_two + string + BigInteger/Geometry/Martix

Всего баллов 41

За задачи в семестре можно набрать 6 баллов в сумме (то есть они весят 60 процентов оценки). Минимально за задачи нужно набрать 2/6 баллов.


Формула для оценки за задачи следующая: МАКС(x*0,2 - 2,2; 0) где x это ваши первичные баллы за задачи

При этом должны быть сданы все required задачи
