# Лекция 20

## 15. Type erasure. Unions

### 4. Unions

Этот функционал мы унаследовали из C. Можно объявить такой тип данных: 

```c++
union U {
  int x;
  double y;
  char c;
}
```

Такой тип позволяет хранить либо int либо double либо char в один момент времени. 

sizeof(U) при этом равен максимуму из размеров всех полей.

Когда мы просто объявляем юнион активным членом становится первый член:

```c++
int main() {
  U u; // в данный момент лежит int
  std::cout << u.x; // 0

  u.d = 3.14; // положили дабл
  std::cout << u.d; // вывело 3.14

  std;:cout << u.x; // UB! (на самом деле будет реинтерпрет каст)
}
```

Проблемы появляются когда хотим положить в union поле с нетривиальными конструкторами/деструкторами, например std::string:

```c++
union U {
  int x;
  double y;
  std::string s;
}
```

Надо объяснить юниону как работать с нашим нетривиальным полем:

```c++
union U {
  int x;
  double y;
  std::string s;

  U(const char* s): s(s) {}
  ~U() {}
}
```

Возникает следующая проблема: после смены активного члена юнион не уничтожает предыдущий активный член. То есть в следующем коде есть утечка памяти:

```c++
union U {
  int x;
  double y;
  std::string s;

  U(const char* s): s(s) {}
  ~U() {}
}

int main() {
  U u = "abc";

  std::cout << u.s;
  u.d = 3.14;
}
```

Чтобы это поправить придется вызвать явно деструктор строки:

```c++
int main() {
  U u = "abc";

  std::cout << u.s;
  u.s.~basic_string();
  u.d = 3.14;
}
```

Следующая проблема: 

```c++
union U {
  int x;
  double y;
  std::string s;

  U(double d): y(d) {}
  U(const char* s): s(s) {}
  ~U() {}
}

int main() {
  U u = 3.14;

  u.s = "abc"; 
}
```

Будет сегфолт потому что под u.s лежит какой-то мусор, а мы у него вызываем оператор=. Придется воспользоваться placement new

Стоит так же упомянуть про анонимные юнионы: 

```c++
int main() {
  union {
    int a;
    double b;
  }

  a = 10;
  b = 20;
}
```

В данном случае переменные a и b будут иметь общую разделяемую память.


### 5. SSO с помощью Union

Пример использования юнионов: оптимизация для малых строк в std::string. Давайте попробуем разобраться что это такое: 

Мы знаем что в стринге хранится поле capacity - реальный размер аллоцируемой памяти. Это поле имеет тип size_t, то есть весит 8 байт. Если у нас сама строка занимает <= 8 байт (на самом деле в стл реализации ставят ограничение даже больше - 15 байт) то нам нет смысла хранить capacity и выделять память под строку на куче. 

Поэтому используется следующая конструкция 

```c++
union {
  size_t capacity_;
  char local_storage_[15];
}
```

Для проверки лежит ли у нас сейчас capacity или нет используются следующий трюк: в полях мы всегда храним указатель на реальное место где лежит стринга. Если этот указатель совпадает с адресом local_storage_ то значит мы в режиме маленькой строки.

### 6. std::variant

Этот тип умеет хранить разные типы на одной и той же памяти. То есть это тот же юнион, только "прокаченный" на c++ лад

```c++
int main() {
  std::variant<int, double, std::string> v;
  v = "abc";

  std::cout << std::get<std::string>(v);
}
```

Если мы "не отгадаем тип" то выкенется исключение: 

```c++
int main() {
  std::variant<int, double, std::string> v;
  v = "abc";

  std::cout << std::get<std::string>(v);
  std::cout << std::get<int>(v); // std::bad_variant_access;
}
```

Тут начинаются приятности: можем просто положить инт и строка корректно уничтожится 

```c++
int main() {
  std::variant<int, double, std::string> v;
  v = "abc";

  std::cout << std::get<std::string>(v);

  v = 5; // int положился и строка корректно уничтожился
}
```

Вкусности продолжаются: 

```c++
int main() {
   std::variant<int, double, std::string> v;
  v = "abc";

  std::cout << std::get<std::string>(v);

  v = 5.0f;
}
```

В вариант положится то, что наиболее предпочтительно. В данном случае double. Работает по правилам перегрузки. 

Есть еще специальная функция std::holds_alternative\<T\> которая отвечает на вопрос "активно ли сейчас в варианте поле T".

Еще есть get по индексу. 


Давайте попробуем понять как это реализовано. Будем рассматривать реализацию на юнионах: 

```c++
template <typename... Types>
struct Variant {

};
```

нужно придумать как завести юнион на все эти типы. Здесь нам пригодится шаблонный юнион (да, так можно)

```c++
template <typename... Types>
union VariadicUnion {};

template <typename Head, typename... Tail>
union VariadicUnion<Head, Tail...> {
  Head head;
  VariadicUnion<Tail...> tail;
}
```

В VariadicUnion лежит либо первый тип, либо юнион из оставшихся. То есть этот юнион хранит один из типов Types 

Теперь можно продолжить писать вариадик:

```c++
template <typename... Types>
struct Variant {
  VariadicUnion<Types...> storage;
};
```

Давайте попробуем сделать get по индексу.

```c++
template <typename... Types>
union VariadicUnion {};

template <typename Head, typename... Tail>
union VariadicUnion<Head, Tail...> {
  Head head;
  VariadicUnion<Tail...> tail;

  template <size_t N>
  auto& get() {
    if constexpr (N == 0) {
      return head;
    } else {
      return tail.template get<N>();
    }
  }
}

template <typename... Types>
struct Variant {
  VariadicUnion<Types...> storage;
  size_t current_index = 0;
};

template <size_t N, typename... Types>
auto& get(Variant<Types...>& v) {
  if (v.current_index == N) {
      return storage.template get<N>();
  }

  throw std::bad_variant_access();
}
```

Теперь давайте сделаем get по типу. Для этого нужно написать метафункцию index_by_type которая из пакета аргументов достает iй тип.

```c++
template <size_t N, typename T, typename... Types>
struct index_by_type_impl {
  static constexpr size_t value = 0;
};

template <size_t N, typename T, typename Head, typename... Tail>
struct index_by_type_impl<N, T, Head, Tail...> {
  static constexpr size_t value = std::is_same_v<T, Head> ? N : index_by_type_impl<N+1, T, Tail...>::value;
};

template <typename T, typename... Types>
static constexpr size_t index_by_type = index_by_type_impl<0, T, Types...>::value;
```
Тогда get по типу реализуется очевидно. 

Теперь самое веселое: как реализовать конструкторы. Ведь нам надо добится того самого волшебного поведения: вариант сам выбирает предпочтительный тип как при перегрузке.

Создадим переменное количество наследников!

Сначала давайте добавим в VariadicUnion функцию put:

```c++
template <typename... Types>
union VariadicUnion {
  // тут бы надо определить что такое get от пустого юниона

  template <typename T>
  void out(const T&) {
    static_assert(false, "fail"); // костыль
  }
};

template <typename Head, typename... Tail>
union VariadicUnion<Head, Tail...> {
  Head head;
  VariadicUnion<Tail...> tail;

  VariadicUnion() = default;
  ~VariadicUnion() = default;

  template <size_t N>
  auto& get() {
    if constexpr (N == 0) {
      return head;
    } else {
      return tail.template get<N>();
    }
  }

  template <typename T>
  void put(const T& value) {
    if constexpr (std::is_same_v<T, Head>) {
      new(&head) T(value);
    } else {
      tail.template put(value);
    }
  }
}
```

```c++
// реализацию VariadicUnion см выше


template <typename T, typename... Types>
struct VariantAlternatives { // новая структура
  using Derived = Variant<Types...>;
  static constexpr index = index_by_type<T, Types...>

  VariantAlternatives(const T& value) {
    auto variant = static_cast<Derived*>(this);
    variant->storage.template put<T>(value);
    ptr->current_index = index;
  }

  VariantAlternatives() = default;
  ~VariantAlternatives() = default;
}

template <typename... Types>
struct Variant
  : public VariantAlternatives<Types, Types...>... { // наследование!
  VariadicUnion<Types...> storage;
  size_t current_index = 0;

  using VariantAlternatives<Types, Types...>::VariantAlternatives...;
};
```

Давайте разбираться как это раскроется вообще. Допустим у нас пакет int, float, string

Тогда мы отнаследовались от следующего набора родителей:

- ```VariantAlternatives<int, int, float, string>```
- ```VariantAlternatives<float, int, float, string>```
- ```VariantAlternatives<string, int, float, string>```


Теперь надо как-то сделать деструктор. Деструктор делается по тому же принципу: заведем метод destroy у VariadicUnion который будет рекурсивно шагать пока не найдет нужный тип и вызовет у него деструктор. Добавим destroy в VariantAlternatives который будет ифаться на индекс и вызывать этот destroy. Куда интересней деструктор самого варианта: 

```c++
template <typename... Types>
struct Variant
  : public VariantAlternatives<Types, Types...>... {

  ~Variant() {
    (VariantAlternatives<Types, Types...>::destroy(), ...);
  }

  VariadicUnion<Types...> storage;
  size_t current_index = 0;

  using VariantAlternatives<Types, Types...>::VariantAlternatives...; // работает с g++-11
};
```

В целом это идейно и есть реализация варианта. Правда у нас тут есть UB. А именно в строчке ```variant->storage.template put<T>(value);```

В этот момент мы в конструкторе родителя, то есть еще не заинитен наследник. То есть поля storage еще не существует))). Выход следующий: сначала отнаследуемся от VariadicStorage а потом от всех наших родителей. 
