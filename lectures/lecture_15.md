# Лекция 15

## 10. Перегрузка new и delete. Аллокаторы.

### 8. Rebinding allocators

Проблема: есть класс list. Он внутри аллоцирует не T а Node\<T\>, а аллокатор у него Allocator\<T\>

Раньше внутри аллокатора был шаблон rebind, который выглядел так: 

```c++
class Allocator {
  template <typename U>
  struct rebind {using other = Allocator<U>; }
}
```

Тогда внутри своего класса (в данном случае List) вы обращались бы к Alloc::rebind\<Node\<T\>::other

Но 

1. Это неудобно
2. Это опять одинаково у всех аллокаторов (почти)

Поэтому функцию rebind перенесли в std::allocator_traits

Давайте на примере List посмотрим как пользоваться rebind:

```c++
template <typename T, typename Alloc = std::allocator<T>>
class List {
 public:
 private:
  using AllocTraits = std::allocator_traits<Alloc>;

  struct Node {
    Node* pref;
    Node* next;
    T value;
  };

  using NodeAlloc = typename AllocTraits::template rebind_alloc<Node>;
  using NodeAllocTraits = typename AllocTraits::template rebind_traits<Node>; // same as std::allocator_trais<NodeAlloc>

  Node* head_ = nullptr;
  Node* tail_ = nullptr;

  NodeAlloc alloc_;

};
```

Теперь NodeAllocTraits можно пользоваться как обычным треитсом.

Проблема следующая: в полях мы храним аллокатор типа NodeAlloc, а передают нам аллокатор для типа T. 

На самом деле проблема легко решается: всякий нормальный аллокатор должен уметь конструироваться от себя же с другим параметром (типом). То есть в примере выше мы просто конструируем наш NodeAlloc от объекта типа Alloc. 

### 9. Копирование и конструирование аллокаторов друг от друга

Посмотрим на примере PoolAlloc: 

```c++
PoolAlloc alloc1;
PoolAlloc alloc2 = alloc1;
```

Не очень понятно что я под этим подразумеваю. Что alloc2 просто должен работать как PoolAlloc и скопировал в себя настройки (размер пула и тд) из alloc1. Или чтобы эти два аллокатора отвечали за один и тот же пул? 

Под равенством  аллокаторов в плюсах обычно понимают следующее: один аллокатор может деаллоцировать объекты, которые аллоцировал второй аллокатор.

Поэтому лучше делать так, как подразумевает ==. (Но вас никто не останавливает делать по-другому, это же плюсы)

Из этого вытекает проблема посерьезней. Давайте посмотрим на следующий код: 

```c++
std::vector<int, PoolAlloc> v1;
std::vector<int, PoolAlloc> v2 = v1;
```

Что в данном случаее делать? Просто скопировать все элементы и сделать новый PoolAllocator? Или скопировать аллокатор так, чтобы он отвечал за тот же пул?

Для этого в allocator_traits есть метод select_on_container_copy_construction

### select_on_container_copy_construction

Этот метод нужно вызывать в конструкторе копирования. Туда передается аллокатор вектора, из которого мы конструируемся и он возвращает новый аллокатор. Поведение этого метода следующее:

1. Если в аллокаторе определен метод select_on_container_copy_construction, то вызывается он
2. Если метод не определен, то возвращается тот же аллокатор

### propagate_on_container_copy_assignemnt

В операторе присваивания копированием же нужно обратиться к propagate_on_container_copy_assignemnt. Если этот юзинг выставлен в true то нам дополнительно к обычным действиям нужно забрать аллокатор (alloc_ = other.alloc_). 

(Аналогично для перемещения)


## 11. Move-семантика и rvalue-ссылки

### 1. Введение и описание проблемы

Давайте посмотрим на функцию swap. Сейчас мы умеем писать ее так:

```c++
template <typename T>
void swap(T& first, T& second) {
  T tmp = first;
  first = second;
  second = tmp;
}
```

Заметим, что во всех трех строках происходит копирование. Если бы мы вызвали такую функцию от какого-нибудь вектора, то мы бы сделали три линейных прохода.

Еще один пример: 

Возьмем нашу реализацию вектора

```c++
int main() {
  vector<std::string> v;
  v.push_back(std::string("test"));
}
```

Тогда конструктор std::string вызовется дважды (потому что push_back вызывает конструктор). Опять лишнее копирование!

Эта проблема решается с помощью метода emplace_back:


```c++
int main() {
  vector<std::string> v;
  v.emplace_back("test");
}
```

Метод emplace_back принимает аргументы, из которых потом конструирует объект:

```c++
template <typename... Args>
void emplace_back(const Args&... args) {
  if (size_ == capacity_) {
    reserve(2 * capacity_);
  }

  new(arr + sz) T(args...);
  size_ += 1;
}
```

Такая реализация уже лучше, но мы все еще копируем аргументы. С текущим инструментарием мы эту проблему не решим. 

Еще один пример можно заметить в методе resize того же вектора. Там мы использовали следующий код:

```c++
std::uninitialized_copy(arr_, arr_ + size_, new_arr);
```

То есть опять делаем лишние копии. А мы хотим научиться просто "перекладывать" объекты.

Еще один примерчик (RVO нас тут не спасает):

```c++
std::string f() {
  return std::string("test");
}

int main() {
  std::vector<std::string> v;
  v.emplace_back(f());
}
```

Переходим к решению проблем

### 2. std::move

Давайте перепишем наш swap:

```c++
template <typename T>
void swap(T& first, T& second) {
  T tmp = std::move(first);
  first = std::move(second);
  second = std::move(tmp);
}
```
И... теперь для большинства стандартных типов (movable-типов) все будет работать за О(1). 

![magic](/lectures/memes/magic.jpg)

Неформально: посмотрим на эту строчку T tmp = std::move(first); Здесь все "ресурсы" объекта first были отданы объекту tmp. Не скопированы, а просто перемещены. 

Сразу важные замечания:

1. Если просто сделать std::move(object), и не присваивать ничего, то object останется таким каким и был (попозже поймем почему)
2. Все стандартные типы гарантируют, что после auto x = std::move(y), y остается валидным (пустым почти наверное). 

### 3. Move-конструкторы, move-операторы присваивания

#### Move-конструкторы

Давайте посмотрим на примере класса String

```c++
class String {
 public:
  String(String&& s): str_(s.str_), size_(s.size_) {
    s.str_ = nullptr;
    s.size_ = 0;
  }

 private:
  char* str_ = nullptr;
  size_t size_ = 0;
}
```

#### move-операторы присваивания

```c++
String operator=(String&& s) {
  String tmp = std::move(s);
  swap(tmp);
  reutrn *this;
}
```

### 4. Автоматическая генерация и правило пяти

move-конструктор генерируется автоматически если нет user-declared move-конструктора и:

1. Нет user-declared copy-конструкторов
2. Нет user-declared операторов присваивания копированием
3. Нет user-declared move-операторов копирования
4. Нет user-declared деструктора

С move-оператором присваивания аналогично. 

Из этого следует очевидное правило пяти :) (как правило трех, только еще два пункта добавили)

А теперь важное: move-конструктор и move-оperator по умолчанию будут просто мувать все поля. Давайте поймем почему такой конструктор будет плохим во многих случаях:


```c++
class String {
 public:
  String(String&& s) {
    s.str_ = std::move(s.str_);
    s.size_ = std::move(s.size_);
  }

 private:
  char* str_ = nullptr;
  size_t size_ = 0;
}
```

std::move от тривиальных объектов просто их копирует. То есть мы не обнулим указатель и размер у строки s, то есть сделаем double free в какой-то момент.

### 5. Исправление методов вектора

Теперь мы можем починить наш push_back в векторе 

```c++
template <typename T>
void Vector<T>::push_back(const T& value) {
  if (size_ == capacity_) {
    reserve(2 * capacity_);
  }

  new(arr + sz) T(value);
  size_ += 1;
}

template <typename T>
void Vector<T>::push_back(T&& value) {
  if (size_ == capacity_) {
    reserve(2 * capacity_);
  }

  new(arr + sz) T(std::move(value));
  size_ += 1;
}  
```

Еще мы можем починить reserve:

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);

  size_t i = 0;
  try {
    for (i < size_; ++i) {
      new(new_arr + i) T(std::move(arr[i]));
    }
  } catch (...) {
    for (size_t j = 0; j < i; ++j) {
      (new_arr + j)->~T();
    }
    delete[] reinterpret_cast<int8_t*>(new_arr);
    throw;
  }

  for (size_t i = 0; i < size_; ++i) {
    (arr + i)->~T();
  }

  delete[] reinterpret_cast<int8_t*>(arr);
  arr = new_arr;
  capacity_ = n;
}
```

Но мы все еще не можем починить emplace_back, потому что мы не знаем какие из аргументов можно мувать, а какие нет. Эту проблему мы сможем решить, когда пройдем std::forward (следующая лекция)


### 5. Определение lvalue и rvalue

Неформально мы уже говорили про эти понятия: lvalue - то что может стоять слева от =, rvalue - то что не может. На самом деле такое определение неверное, причем в обе стороны

Пример в одну сторону: объект, у которого не определен оператор присваивания не может стоять слева от =, хотя он lvalue

Пример в другую сторону: BitReference в vector\<bool\>. Это rvalue (временно созданный объект) однако он стоял слева от =

```c++
BitRef operator[](size_t i) {
  return BitReference(arr_ + i / 8, i % 8);
}
```


Прикол от разработчиков стандарта: rvalue-ссылка далеко не всегда является rvalue. (Обычные ссылки называются lvalue)

Пока мы поговорим только про lvalue и rvalue (на самом деле prvalue, потому что rvalue = prvalue + xvalue). 


Главное, что стоит понимать: понятия rvalue и lvalue применимы к выражениям. То есть rvalue и lvalue это типы выражений, а не объектов или типов. Переходим к формальному определению

Приводить здесь полное определение не будем, сошлемся на [статью](https://en.cppreference.com/w/cpp/language/value_category)

Самое важное: идентификаторы и литералы, а так же вызовы функций

Определение prvalue это на самом деле определение rvalue из c++03, но в c++11 ввели rvalue-ссылки. Они так называются, потому что когда их возвращают из вызова функции (или в касте) выражение считается rvalue (а обычные ссылки - lvalue).

Для rvalue-ссылок в c++11 ввели xvalue (expired value) и теперь rvalue = prvalue + xvalue.
