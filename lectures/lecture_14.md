# Лекция 14

## 10. Перегрузка new и delete. Аллокаторы.

### 1. Перегрузка стандартных new и delete

Давайте посмотрим на такой пример:

```c++
struct S {
  int x = 0;
}

int main() {
  S* ptr = new S(); // 1
}
```

Как мы уже знаем в строке 1 должна выделиться память и вызваться конструктор S (в данном случае конструктор по умолчанию)

Перегрузить можно только само выделение памяти. Давайте это сделаем

```c++
void* operator new(size_t n) {
  std::cout << "We are in new";
  return malloc(n);
}

void operator delete(void* p) noexcept {
  std::cout << "We are in delete";
  free(p);
}

struct S {
  int x = 0;
}

int main() {
  S* ptr = new S(); // 1
  delete s;
}
```

Таким образом мы смогли перезгрузить глобальный оператор new и delete. Но это не все операторы new и delete, потому что есть еще их версии для массивов

```c++
void* operator new[](size_t n) {
  std::cout << "We are in new array";
  return malloc(n);
}

void operator delete[](void* p) noexcept {
  std::cout << "We are in delete";
  free(p);
}
```

Давайте убедимся что наши перегрузки действительно работают

```c++
void* operator new(size_t n) {
  std::cout << "We are in new array";
  return malloc(n);
}

void operator delete(void* p) noexcept {
  std::cout << "We are in delete";
  free(p);
}

int main() {
  std::vector<int> v(10);
  v[0] = 10;
}
```

***Note*** Вектор использует new без \[\], позже поймем почему

То есть перезгрузка работает. Теперь давайте поймем, почему такая перегрузка плохая 

Оператор new умеет кидать исключение, если он не смог выделить память, поэтому:

```c++
void* operator new(size_t n) {
  std::cout << "We are in new";

  void* ptr = malloc(n);

  if (ptr == nullptr) {
    throw std::bad_alloc();
  }

  return ptr;
}

void operator delete(void* p) {
  std::cout << "We are in delete";
  free(p);
}
```

На самом деле стандартный оператор new в случае провала сначала пытается вызвать функцию std::new_handler, и только в случае ее провала бросает исключение.

Еще одно важное замечание: если вы запросите 0 байт у new, то он выделит 1 байт

### 2. Другие формы оператора new/delete

#### 1. Перегрузка оператора new для конкретного типа

```c++
struct S {
  static void* operator new(size_t n) {
    std::cout << "We are in new for S";

    void* ptr = malloc(n);

    if (ptr == nullptr) {
      throw std::bad_alloc();
    }

    return ptr;
  }

  static void operator delete(void* p) {
    std::cout << "We are in delete for struct S";
    free(p);
  }
}
```

***Note*** Статик будет по умолчанию, но лучше не забывать писать

***Note*** При выборе какой оператор выбрать для типа, предпочтение будет отдано оператору, который объявлен внутри класса

#### 2. placament-new

Давайте составим вот такой искуственный пример:

```c++
struct S {
 private:
  S() {
    std::cout << "Private construct S\n";
  }

 public:
  static void* operator new(size_t n) {
    std::cout << "We are in new for S";

    void* ptr = malloc(n);

    if (ptr == nullptr) {
      throw std::bad_alloc();
    }

    return ptr;
  }

  static void operator delete(void* p) {
    std::cout << "We are in delete for struct S";
    free(p);
  }
}

int main() {
  S* p = reinterpret_cast<S*>(opeartor new(sizeof(S)));
  new(p) S();
  operator delete(p);
}
```

Это не сработает, потому что если для типа определен кастомный оператор new/delete то placement new сгенерирован не будем
Давайте удалим перегрузку

```c++
struct S {
 private:
  S() {
    std::cout << "Private construct S\n";
  }

 public:
}

int main() {
  S* p = reinterpret_cast<S*>(opeartor new(sizeof(S)));
  new(p) S();
  operator delete(p);
}
```

Теперь CE потому что конструктор приватный. Давайте перегрузим placement new и сделаем его другом


```c++
struct S {
  friend void* operator new(size_t, S*);
 private:
  S() {
    std::cout << "Private construct S\n";
  }

 public:
}

void* operator new(size_t, S* p) {
  return p;
}

int main() {
  S* p = reinterpret_cast<S*>(opeartor new(sizeof(S)));
  new(p) S();
  operator delete(p);
}
```

Перегрузка именно такая, потому что часть с вызовом конструкторов мы перегружать не можем. А выделять память в placement new уже не нужно.

Это опять не работает, потому что функция operator new конструктор не вызывает



```c++
struct S {
  S() {
    std::cout << "Private construct S\n";
  }
}

void* operator new(size_t, S* p) {
  return p;
}

int main() {
  S* p = reinterpret_cast<S*>(opeartor new(sizeof(S)));
  new(p) S();
  operator delete(p);
}
```

Кастомный оператор delete при этом компилятор не вызывает, кроме случая, когда в операторе new произойдет исключение

Пример:

```c++
struct S {
  S() {
    throw 1;
  }
}

void* operator new(size_t n, double d) {
  std::cout << "custom new with d: " << d << std::endl;
  return malloc(n);
}

void operator delete(void* p, double d) {
  std::cout << "custom delete with d: " << d << std::endl;
  return free(p);
}

int main() {
  try {
    S* p = new(3.14) S();
  } catch (int) {
    
  }
}
```

### 3. Идея аллокаторов

На данный момент мы знаем, что в c++ память выделяется с помощью оператора new, который в свою очередь обращается к malloc.

То есть new это абстракция над выделением памяти в Си. Но это все еще довольно низкоуровневая вещь, плюс с ней не очень удобно работать. Например: хотим для типа MyType чтобы std::vector выделял память одним способом, а std::set другим. С помощью перегрузок оператора new этого не добиться

Поэтому в c++ появилась еще более высокоуровневая абстракция над выделением памяти под названием аллокатор. 
Аллокатор это способ переопределить выделение памяти до момента обращения к оператору new. 

### 4. std::allocator

Давайте посмотрим на реализацию стандартного аллокатора, который используется по умолчанию во всех контейнерах. Он очевидно должен просто обращаться к new/delete 

```c++
template <typename T>
struct Allocator {
  T* allocate(size_t n) {
    return ::operator new(n * sizeof(T));
  }

  void deallocate(T* ptr, size_t) {
    ::operator delete(ptr);
  }

  template <typename... Args>
  void construct(T* ptr, const Args&...) { // здесь на самом деле должно быть Args&&
    new(ptr) T(args...); // а здесь std::forward(args)
  }

  void destroy(T* ptr) noexcept {
    ptr->~T();
  }
}
```

Конечно в нем еще есть куча using'ов, но это уже не так интересно. 

Как можно заметить аллокатор (как и new) разделяет создание объекта и выделение памяти, правда в случае аллокатора часть с вызовом конструктора можно перегрузить. 

### 5. Пример нестандартного аллокатора: PoolAllocator

В чем идея: давайте в конструкторе аллокатора запросим сразу много памяти. 
При выделении памяти будем отдавать кусочек, а при вызове deallocate делать ничего не будем 


```c++
template <typename T>
class PoolAllocator {
public:
  PoolAllocator() {
    pool_ = ::operator new(pool_size);
    pool_begin_ = pool_;
  }

  ~PoolAllocator() {
    ::operator delete(pool_);
  }

  T* allocate(size_t n) {
    size_t bytes = n * sizeof(T);
    auto memory_to_return = pool_;
    pool_ += bytes;
    return reinterpret_cast<T*>(memory_to_return);
  }

  void deallocate(T* ptr, size_t) {
    return;
  }

  template <typename... Args>
  void construct(T* ptr, const Args&...) { // здесь на самом деле должно быть Args&&
    new(ptr) T(args...); // а здесь std::forward(args)
  }

  void destroy(T* ptr) noexcept {
    ptr->~T();
  }

private:
  const size_t pool_size = 1000000;
  uint8_t* pool_;
  uint8_t pool_begin_;
}
```

А еще можно в аллокаторе выделить память на стеке.

### 6. allocator_traits

Большинство методов у всех аллокаторов будут одинаковые (например construct и destroy) а еще внутри есть куча using, поэтому в c++ была добавлена специальная обертка allocator_traits

allocator_traits это структура, которая шаблонным параметром принимает класс вашего аллокатора

Почти все методы и внутренние юзинги работают по прицнипу "взять у аллокатора если есть, если нет сгенерировать автоматически"

Лучше всего посмотреть на описание и список методов [тут](https://en.cppreference.com/w/cpp/memory/allocator_traits)

### 7. Правильная реализация контейнеров

Давайте вспомним нашу реализацию вектора. Теперь у нее появляется новый шаблонный параметр Alloc

```c++
template <typename T, typename Alloc = std::allocator<T>>
class Vector {
 public:
  Vector(size_t n, const T& value = (), const Alloc& allocator = Alloc());

  T& operator[](size_t i) {
    return arr_[i];
  }

  const T& operator[](size_t i) const {
    return arr_[i];
  }

  T& at(size_t i) {
    if (i >= size_) {
      throw std::out_of_range("...");
    }

    return arr_[i];
  }

  const T& at(size_t i) const {
    if (i >= size_) {
      throw std::out_of_range("...");
    }

    return arr_[i];
  }

  size_t size(return size_; )
  size_t capacity(return capacity_; )

  void resize(size_t n, const T& value = T());

  void reserve(size_t n);

 private:
  using AllocTraits = std::allocator_traits<Alloc>; // это не обязательно, но так будет удобней

  T* arr_;
  size_t size_;
  size_t capacity_;
  Alloc alloc_; // Сохраняем к себе!
}
```

А теперь поймем как правильно обращаться к аллокатору на примере метода reserve:

***

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  // T* new_arr = new T[n]; // На неуд
  // T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]); На удос
  // T* new_arr = alloc_.allocate(n); На хор
  T* new_arr = AllocTraits::allocate(alloc_, n);  // На отл

  size_t i = 0;
  try {
    for (;i < size_; ++i) {
      AllocTraits::construct(alloc_, new_arr + i, arr_[i]); // здесь std::move(arr_[i])
    } 
  } catch (...) {
    for (size_t j = 0; j < i; ++j) {
      AllocTraits::destroy(alloc_, new_arr + j);
    }
    AllocTraits::dallocate(alloc_, n);
    throw;
  }

  for (size_t i = 0; i < size_; ++i) {
    AllocTraits::destroy(alloc_, arr_ + i);
  }

  AllocTraits:deallocate(alloc_, arr_, capacity_)

  arr_ = new_arr;
  capacity_ = n;
}
```

Тоже самое надо проделать во всех остальных методах вектора
