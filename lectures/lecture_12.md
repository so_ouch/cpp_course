# Лекция 12

## 8. Итераторы

### 3. Категории итераторов

a. Input один проход на чтение 

std::istream

b. Output один проход на ввод


1. ForwardIterator ++

forward_list
unordered_map/set

2. BidirectionalIterator --

list, map/set

3. RandomAccessIterator += -= <> it1 - it2

vector, deque

4. ContiguousIterator гарантия на непрерывность памяти

### 4. std::advance, std::distance

#### std::advance 

Это функция "продвинуть". Давайте посмотрим как она работает

```c++
std::list<int> v = {1, 2, 3, 4, 5};
std::list<int>::iterator it = v.begin();
std::advance(it, 3); 
std::cout << *it; // 4 
```

#### std::distance 

Тут все просто, она считает расстояние от одного итератора до другого

***Note*** если второй итератор не достижим из первого, то это UB

#### Реализация этих функций

В чем посыл? В зависимости от типа итератора, у нас должно быть разное время работы. Например, для RandomAccess итератора время будет О(1), а для Forward будет O(n)

#### std::iterator_traits

Чтобы понять, какого типа нам дали итератор, нам нужно воспользоваться структурой [std::iterator_traits](https://en.cppreference.com/w/cpp/iterator/iterator_traits)



#### std::my_advance

Случай с отрицательным n рассматривать не будем

```c++
#include <iterator>
template <typename Iterator>
void my_advance(Iterator& iter, int n) {
  if (std::is_same_v<typename std::iterator_traits<Iterator>::iterator_category, std::random_access_iterator_tag>) {
    iter += n;
  } else {
    for (int i = 0; i < n; ++i) {
      ++iter;
    }
  }
}

int main() {
  std::list<int> list = {1, 2, 3};
  std::list<int>::iterator it = v.begin();
  my_advance(it, 3);
}
```

Но, ничего не работает. 

<details> 
  <summary>Почему?</summary>
   Ответ: хоть мы и не вызываем строку iter += n, но мы пытаемся ее скомпилировать. А итератор листа не умеет делать  += n;
</details>


<details> 
  <summary>Как нам это решить?</summary>
   Ответ: перезгрузка функций!
</details>

```c++
#include <iterator>

template <typename Iterator, typename IteratorCategory>
void helper(Iterator& iter, int n, IteratorCategory) {
  for (int i = 0; i < n; ++i) {
    ++iter;
  }
}

template <typename Iterator>
void helper(Iterator& iter, int n, std::random_access_iterator_tag) {
  iter += n;
}

template <typename Iterator>
void my_advance(Iterator& iter, int n) {
  helper(iter, n, typename std::iterator_traits<Iterator>::iterator_category());
}

int main() {
  std::list<int> list = {1, 2, 3};
  std::list<int>::iterator it = v.begin();
  my_advance(it, 3);
}
```

Начиная с c++17 это решается с помощью if constexpr, но про это потом) 


#### std::my_distance

Домашнее задание :)

### 5. Const итераторы

Это итераторы, которые не позволяют менять значение под собой

```c++
int main() {
  std::list<int> list = {1, 2, 3};
  std::list<int>::const_iterator it = v.cbegin();
  *it = 2; // не скомпилируется
}
```

***Note*** чтобы не писать такие длинные названия типов, можно пользоваться auto: 

```c++
auto it = v.cbegin();
```

О том как оно устроено внутри мы будем говорить позже

### 6. Как реализовывать свой итератор

```c++
template <typename T>
class Vector {
 public:
  class iterator;

  iterator begin() const {
    return iterator(arr_);
  }

  iterator end() const {
    return iterator(arr_ + size_);
  }
 private:
  T* arr_;
  size_t size_;
};

template <typename T>
class Vector<T>::iterator {
 public:
  T& operator*() {
    return *ptr;
  }

  iterator& opeartor++() {
    ++ptr;
    return *this;
  }

  T* opeartor->() {
    return ptr;
  }

 private:
  T* ptr;
};
```

Как теперь написать const iterator?

```c++
template <typename T>
class Vector {
 public:
  template <bool IsConst>
  class common_iterator;

  using iterator = common_iterator<false>;
  using const_iterator = common_iterator<true>;

  common_iterator begin() const {
    return common_iterator(arr_);
  }

  common_iterator end() const {
    return common_iterator(arr_ + size_);
  }
 private:
  T* arr_;
  size_t size_;
};

template <typename T>
template <bool IsConst>
class Vector<T>::common_iterator<IsConst> {
 public:
  using Type = std::conditional_t<IsConst, const T, T>;
  Type& operator*() {
    return *ptr;
  }

  common_iterator<IsConst>& opeartor++() {
    ++ptr;
    return *this;
  }

  Type* opeartor->() {
    return ptr;
  }

 private:
   Type* ptr;
};
```

### 7. reverse итераторы

Делает все наоборот:

```c++
template <typename Iterator>
struct reverse_iterator {
 public:
  reverse_iterator<Iterator>& opeartor++() {
    --iter;
    return *this;
  }  
 private:
  Iterator iter;
}
```

```c++
int main() {
  std::list<int> list = {1, 2, 3};
  for (auto it = v.rbegin(); it != v.rend(); ++it) {
    std::cout << *it;
  }
}
```

### 8. std::copy и std::copy_if

[cpp_ref](https://en.cppreference.com/w/cpp/algorithm/copy)


Почему следующий код это UB?

```c++
std::list<int> l = {1, 2, 3, 4};
std::vector<int> v;
std::copy(l.begin(), l.end(), v.begin());
```

Потому что функция std::copy в тупую пишет и не проверяет размер. 

Для того чтобы спокойно писать в контейнер нам нужен output итератор, а итераторы стандартных контейнеров такими не являются

### 9. output итераторы и адаптеры

Рассмотрим адаптеры из стандартной библиотеки, который позволяет получить output итератор из стандартного контейнера:

[std::back_inserter](https://en.cppreference.com/w/cpp/iterator/back_inserter)

```c++
std::list<int> l = {1, 2, 3, 4};
std::vector<int> v;
std::copy(l.begin(), l.end(), std::back_inserter(v));
```

Давайте посмотрим на класс  std::back_insert_iterator:

```c++
template <typename Container>
class back_insert_iterator {
 public:
  back_insert_iterator(Container& container): container(container) {}

  back_insert_iterator<Container>& operator++() {
    return *this;
  }

  back_insert_iterator<Container>& operator*() {
    return *this;
  }

  back_insert_iterator<Container>& operator=(const typename container::value_type& value) {
    container.push_back(value);
    return *this;
  }
 private:
  Container& container;
}

template <typename Container>
back_insert_iterator<Container< back_inserter(Container& container) {
  return back_insert_iterator<Container>(container);
}

```

Также есть front_insert_iterator(push_front) и просто insert_iterator(insert)

### 10. Stream итераторы 

Давайте заведем итератор на cin

```c++
std::istream_iterator<int> it(std::cin);

for (int i = 0; i < 3; ++i) {
  std::cout << *it;
  ++it;
}
```

Давайте посмотрим на примерную реализацию такого итератора

```c++
template <typename T>
class istream_iterator {
 public:
  istream_iterator(std::istream& in): in_(in) {
    in_ >> value;
  }

  istream_iterator<T>& operator++() {
    in_ >> value;
  }

  T& opeartor*() {
    return value;
  }

 private:
  T value;
  std::istream& in_;
}
```
