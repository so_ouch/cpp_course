# Лекция 16

## 11. Move-семантика и rvalue-ссылки

### 7. rvalue-ссылки и правила их инициализации

Будем смотреть на примере int.

Вспомним сначала про lvalue-ссылки

```c++
int main() {
  int x = 0;
  int& y = x;
}
```

Эта запись означает что y это другое имя для x

Но написать вот так: 

```c++
int main() {
  int& y = 1;
}
```

мы не можем, потому что мы не можем инициализировать не константную lvalue-ссылку с помощью rvalue. А вот константную можем: 

```c++
int main() {
  const int& y = 1;
}
```

С rvalue-ссылками все наоборот: мы не можем инициализировать rvalue-ссылку с помощью lvalue. То есть следующий код не скомпилируется: 

```c++
int main() {
  int x = 0;
  int&& y = x;
}
```

А вот такой уже будет работать:

```c++
int main() {
  int&& y = 0;
}
```

И вот такой тоже:

```c++
int main() {
  int x = 1;
  int&& y = 0;
  y = x;
}
```

Вопрос века: будет ли работать следующий код? 

```c++
int main() {
  int&& x = 0;
  int& z = x;
}
```

<details> 
  <summary>Спойлер</summary>
   Ответ: да, потому что x это lvalue
</details>

А такой?


```c++
int main() {
  int&& x = 0;
  int&& y = x;
}
```

<details> 
  <summary>Спойлер</summary>
   Ответ: нет, потому что x это lvalue
</details>


Чтобы создать rvalue-ссылку на уже существующий объект, нам нужна функция std::move:

```c++
int main() {
  int&& x = 0;
  int&& y = std::move(x);
}
```

Теперь можно разобраться как работает перегрузка конструкторов. Для этого вспомним наш класс string:

```c++
class String {
 public:
  // 1
  String(const String& s) {
    str_ = new char[s.size_]
    memcpy(str_, s.str_, size_);
  }
  // 2
  String(String&& s): str_(s.str_), size_(s.size_) {
    s.str_ = nullptr;
    s.size_ = 0;
  }

 private:
  char* str_ = nullptr;
  size_t size_ = 0;
}
```

Если вызываться от lvalue то вызовется первый конструктор, потому что String&& s нельзя проинициализировать с помощью lvalue. А вот для rvalue вызовется второй конструктор.

### 8. Универсальные ссылки и правила сжатия ссылок

Хотим реализовать функцию std::move. Но есть проблема: мы не умеем писать функции, которые могут принимать и lvalue и rvalue:

```c++
foo(const type& x) // принимает и то и то, но const
foo(type& x) // принимает только lvalue
foo(type&& x) // принимает только rvalue
```

Разработчики стандарта тоже столкнулись с этой проблемой и придумали следующий костыль: 

```c++
template <typename T>
void f(T&& x) {

}
```

Вот в таком контексте x будет являться универсальной ссылкой

Цитата Мейерса: "If a variable or parameter is declared to have type T&& for some deduced type T, that variable or parameter is a universal reference."

Чтобы понять какие у этих ссылок правила вывода, сначала разберемся с правилом сжатия ссылок (reference collapsing)

В c++ не допускается взятие ссылки на ссылку, но такое возникакает при шаблонных подстановках. 

Например:

```c++
template <typename T>
void foo(T x) {
  T& y = x;
}

int main() {
  int x = 0;
  foo<int&>(x);
}
```

При подстановке получится int& & y = x, что запрещено, потому компилятор преобразует это в просто int&. До c++11 такое поведение не было стандартизированно, а вот с появлением rvalue-ссылок правило пришлось ввести. Оно очень простое: одиночный & всегда побеждает. То есть:

1. & и & это &
2. && и & это &
3. & и && это &
4. && и && это &&


Теперь можно понять какие правила вывода работают для универсальных ссылок:

```c++
template <typename SomeType>
void f(SomeType&& x) {}

int main() {
  f(5); // SomeType = int, decltype(x) = int&&
  int y = 5;
  f(y); // SomeType = int&!, decltype(x) = int&;
}
```

Вывод: тип будет lvalue-ссылкой или rvalue-ссылкой в зависимости от типа value которое передали в функцию

Два важных замечания:

***Note*** T&& является универсальной ссылкой, только если T это шаблонный параметр этой функции. (Например в контексте push_back в векторе T&& value не является универсальной ссылкой, так как T это шаблонный параметр класса)

***Note** При выборе перегрузки универсальные ссылки считаются предпочтительней

Теперь можно перейти к реализации std::move

### 9. Реализация std::move

Хотим функцию, которая будет 

Заготовка у нас будет такая: 


```c++
template <typename T>
? move(T&& x) noexcept {

}
```

Давайте определимся с возвращаемым типом. Написать T&& нельзя, потому что нам нужно всегда возвращать rvalue-ссылку, а с учетом правила сжатий ссылок такого не получится. Вспоминаем про type-traits!

```c++
template <typename T>
std::remove_reference_t<T>&& move(T&& x) noexcept {
  return ...
}
```

Теперь нужно понять что нужно написать после слова return. 

Написать просто return x нельзя (пытаемся инициализировать rvalue-ссылку с помощью lvalue). Поэтому придется написать следующее:

```c++
template <typename T>
std::remove_reference_t<T>&& move(T&& x) noexcept {
  return static_cast<std::remove_reference_t<T>&&>(x);
}
```

Итого мы: 

1. Научились делать push_back
2. Научились делать swap
3. Научились передавать временные объекты без копирования

#### Когда не надо писать std::move

Допустим у нас есть функция get() которая возвращает временный объект (то есть это rvalue)

Тогда не нужно писать f(std::move(get())), можно просто написать f(get())

### 10. Perfect-forwarding problem и функция std::forward

Давайте чинить emplace_back нашего вектора 


 Сейчас он выглядит так:

```c++
template <typename... Args>
void emplace_back(const Args&... args) {
  if (size_ == capacity_) {
    reserve(2 * capacity_);
  }

  new(arr + sz) T(args...);
  size_ += 1;
}
```

Очевидно что надо поменять тип аргументов на универсальную ссылку


```c++
template <typename... Args>
void emplace_back(Args&& args) {
  if (size_ == capacity_) {
    reserve(2 * capacity_);
  }

  new(arr + sz) T(args...);
  size_ += 1;
}
```

Теперь тип каждого аргумента в "пачке" будет либо lvalue ref либо rvalue ref (хоть тип args и lvalue)

Написать std::move(args...) нельзя, так как все аргументы мувнуться 


Чтобы решить эту проблему используется функция std::forward


```c++
template <typename... Args>
void emplace_back(Args&& args) {
  if (size_ == capacity_) {
    reserve(2 * capacity_);
  }

  new(arr + sz) T(std::forward<Args>(args)...);
  size_ += 1;
}
```

Заметьте, что шаблонный параметр надо указывать явно.

Давайте ее напишем

```c++
template <typename T>
T&& forward(std::remove_reference_t<T>& x) noexcept {
  return static_cast<T&&>(x);
}
```

Давайте разберем случаи:

Первый:

arg = lvalue
Arg = type&
decltype(arg) = type&
T = type&
x = type&
T&& = type&


Второй:

arg = rvalue
Arg = type
decltype(arg) = type&&
T = type
x = type&
T&& = type&&

Теперь мы муваем аргументы, которые можем. А остальные копируем

### 11. move_if_noexcept

Опять обратимся к нашей реализации вектора

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);

  size_t i = 0;
  try {
    for (i < size_; ++i) {
      new(new_arr + i) T(std::move(arr_[i]));
    }
  } catch (...) {
    for (size_t j = 0; j < i; ++j) {
      (new_arr + j)->~T();
    }
    delete[] reinterpret_cast<int8_t*>(new_arr);
    throw;
  }

  for (size_t i = 0; i < size_; ++i) {
    (arr_ + i)->~T();
  }

  delete[] reinterpret_cast<int8_t*>(arr_);
  arr_ = new_arr;

  capacity_ = n;
}
```

Если во время std::move(arr[i]) произойдет исключение (исключение во время вызова конструктора перемещения), то у нас будут проблемы. Поэтому в стандартной библиотеке используется функция std::move_if_noexcept (по названию довольно очевидно что она делает)


### 12. reference qualifiers

Вспомним про const в методах класса. Тогда у нас была проблема с оператором [], и const помогал нам выбрать нужную перегрузку. Так же с ссылками:

```c++
struct S {
  void f() & {
    std::cout << 1;
  }

  void f() && {
    std::cout << 2;
  }
}

int main() {
  S s;
  s.f()
  std::move(s).f();
}
```

Пример применения: 

Есть у нас класс BigInteger. Есть для него оператор +. Мы хотим чтобы выражения вида a + b = 5 не работали. До C++ 11 это решалось тем, что operator+ возвращал const BigInteger. Начиная с c++11 нужно просто оператор присваивания пометить rvalue qualifier:

```c++
struct BigInteger {
  BigInteger& opeartor=(const BigInteger&) &;
}
```

***Note*** если вообще не ставить qualifier то метод будет одинаково работать для обоих видов value.

## 12. Вывод типов

### 1. Ключевое слово auto

В c++ есть ключевое слово auto, которое позволяет в некоторых местах не писать тип. Наша задача разобраться как это работает

Зачем это вообще нужно: 

```c++
int main() {
  std::map<std::string, std::int> m;

  for (std::map<std::string, std::int>::iterator it = m.begin(); it != m.end(); ++it) {
    ...
  }
}
```

VS

```c++
int main() {
  std::map<std::string, std::int> m;

  for (auto it = m.begin(); it != m.end(); ++it) {
    ...
  }
}
```

Еще пример:

```c++
int main() {
  std::map<std::string, std::int> m;

  for (const std::pair<std::string, int>& item : m) {
    ...
  }
}
```

item здесь будут копироваться, потому что внутри контейнера map лежат пары вида std::pair\<const std::string, int\>. Соответственно в этом случае тоже хорошо бы использовать auto

Еще одна причина использовать auto: типы в коде могут поменяться и без auto это превратится в огромный рефакторинг

Auto работает так же как вывод шаблонного типа. Auto понятное дело можно комбинировать со ссылками и константностью:

```c++
int main() {
  std::map<std::string, std::int> m;

  for (const auto& item : m) {
    ...
  }
}
```

***Note*** с auto как и с T работает отбрасывание ссылок, то есть при auto x = "чему-то со ссылкой" ссылка отбросится. Например: 

```c++
int& foo(int& x) {return x; }

int main() {
  int x = 10;
  auto y = foo(x);
  ++y;
}
```

Рассмотрим такой код: 

```c++
int& foo(int& x) {return x; }

int main() {
  int x = 10;
  auto&& y = foo(x);
  ++y;
}
```

И тут auto&& работает как универсальная ссылка, то есть тут работают все те же правила.

### 2. Ключевое слово decltype

decltype это такая метафункция, которая в compile-time возвращает тип выражения

На decltype(obj) можно так же навешивать различные модификаторы типов (прям как с шаблонами)

decltype так же можно брать от выражений

```c++
int x;
decltype(x++) u = x;
```

***Note*** x не увеличится!

Правило работы с выражениями такое:

Если expr это не идентификатор, то
 
+ Если expr это prvalue типа T, то decltype(expr) = T  (decltype(x++))
+ Если expr это lvalue типа T, то decltype(expr) = T& (decltype(++x))
+ Если expr это xvalue типа T, то decltype(expr) = T&& (decltype(std::move(x)))

Интересный пример 

```c++
decltype(throw 1)* p = nullptr; // void* p = nullptr;
```

### 3. Вывод типа для возвращаемого значения функций

auto можно использовать при объявлении функции:

```c++
auto f(int& x) {
  return x;
}
```

Пример сложней:

```c++
auto f(int& x) {
  if (x > 0) {
    return x;
  }

  return 0.5;
}
```

Тут мы заставили компилятор в рантайме определять тип, что плохо, поэтому CE


Посмотрим на следующую проблему:

```c++
template <typename T>
... g(const T& x) {
  return some_func(x)
}
```

Хотим всегда возвращать тип такой, какой вернет some_func, но допустим что some_func умеет возвращать ссылку или не ссылку для некоторых типов. Поэтому auto не поможет

Написать вот так:

```c++
template <typename T>
decltype(some_func(x)) g(const T& x) {
  return some_func(x)
}
```

Мы тоже не можем потому что x еще не объявлен, поэтому в c++ есть еще такой синтаксис:

```c++
template <typename T>
auto g(const T& x) -> decltype(f(x)) {
  return some_func(x)
}
```

Еще пример:

```c++
template <typename Container>
auto get(const Container& c, size_t i) -> decltype(c[i]) {
  return c[i];
}
```

Проблема такая: не всегда обращение к контейнеру по индексу возвращает ссылку (вектор булов), поэтому приходится вот так извращаться с возвращаемым типом

Начиная с c++14 этот синтаксис упростили

```c++
template <typename Container>
decltype(auto) get(const Container& c, size_t i) {
  return c[i];
}
```

По сути это заставит компилятор выводить тип не по правилам auto а по правилам decltype

***Note*** decltype(auto) можно так же использовать в других выражениях (при инициализации например)
