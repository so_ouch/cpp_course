# Лекция 19

## 14. Лямбда функции

### 6. std::function

Это класс, объектам которого можно присваивать любые объекты, которые имеют круглые скобоки (указатели на фукнции, функторы, лямбды)

```c++
int f(int x, int y) {
  return x + y;
}

int main() {
  std::function<int(int, int)> f = foo;
  f(10, 10);
}
```

std::function хранит в себе нечто, в чем сохраняется наша обычная функция. Интересности начинаются тут:

```c++
int foo(int x, int y) {
  return x + y;
}

struct S {
  int operator()(int x, int y) const {
    return x / y;
  }
}

int main() {
  std::function<int(int, int)> f = foo;
  f(10, 10);

  f = S();
  f(10, 10); // мувнул в себя S

  S s;
  f = s;
  f(10, 10); // скопировал в себя S

  f = [](int x, int y) {return x * y; };
}
```

То есть можно динамически подменять хранимую "функцию".

Давайте посмотрим на цппреф https://en.cppreference.com/w/cpp/utility/functional/function

function - полиморфная обертка над функциями. Объект function можно хранить, копировать, вызывать.


std::function можно использовать с указателями на методы: 

```c++
struct S {
  void foo(int x) {
    std::cout << x + 1;
  }
};

int main() {
  void S(::* p)(int) = &S::goo;
  S s;
  (s.*p)(5);


  std::function<void(S&, int)> f = &S::goo;
  f(s, 5);
}
```

Нужно передавать еще ссылку на S, так как метод все таки вызывается у объекта.

Обсуждать реализацию function мы (пока) не будем. 

## 15. Type erasure. Unions

### 1. Базовая идея, std::any.

std::any это базовый пример стирания типа: он позволяет хранить в себе что угодно:

```c++
int main() {
    std::any a = 5;

    std::vector<int> v = {1, 2, 3, 4 ,5};
    a = std::move(v);

    a = 'a';
}
```

Чтобы получить хранимый объект необходимо воспользоваться any_cast. Тут то и всплывает то, что в плюсах невозможна динамическая типизация:

```c++
int main() {
    std::any a = 5;

    std::vector<int> v = {1, 2, 3, 4 ,5};
    a = std::move(v);

    a = 'a';

    std::cout << std::any_cast<char>(a);
}
```

Если мы "не угадаем" тип, то будет exception std::bad_any_cast.

Давайте посмотрим на методы std::any: https://en.cppreference.com/w/cpp/utility/any

Попробуем идейно понять реализацию std::any.

```c++
struct Any {
  template <typename U>
  Any(const U& object) {
    // ???
  }

  ~Any() {
    // ???
  }
}
```

Главная проблема на самом деле в удалении. Нам нужно корректно удалять объекты в any, а они все разные

Здесь нам как раз поможет type erasure:

```c++
struct Any {
  struct Base {
    virtual ~Base() = default;
  };

  template <typename T>
  struct Derived: public Base {
    T object;
    Derived(const T& object): object(object) {}
  };

  // TODO: fix copy-assignment operator for any
  template <typename U>
  Any(const U& object): ptr(new Derived<U>(object)) {}

  template <typename U>
  Any& operator=(const U& object) {
    delete ptr;
    ptr = new Derived<U>(object);
  }

  ~Any() {
    delete ptr;
  }

  Base* ptr = nullptr;
}
```

Теперь давайте попробуем разобраться с конструктором копирования. Это не так просто и нам понадобится отдельный метод в нашем Derived

```c++
struct Any {
  struct Base {
    virtual ~Base() = default;

    virtual Base* clone() = 0;
  };

  template <typename T>
  struct Derived: public Base {
    T object;
    Derived(const T& object): object(object) {}

    Base* clone() override {
      return new Derived<T>(object);
    }
  };

  template <typename U>
  Any(const U& object): ptr(new Derived<U>(object)) {}

  Any(const Any& other): ptr(ptr->clone()) {}

  // TODO: fix copy-assignment operator for any
  template <typename U>
  Any& operator=(const U& object) {
    delete ptr;
    ptr = new Derived<U>(object);
  }

  ~Any() {
    delete ptr;
  }

  Base* ptr = nullptr;
}
```

Теперь должно быть чуть лучше понятно как реализован std::function: там применяется тот же прием с Base и Derived. Просто для Derived делаются специализации. 

Эта же идиома применима к shared_ptr

### 2. Кастомный Deleter в shared_ptr

Вспомним что у нас был ControlBlock. Давайте к нему применим тот же прием что и в std::any:

```c++
struct BaseControlBlock {
  T object;
  size_t shared_count;
  size_t weak_count;

  BaseControlBlock(const T&, size_t, size_t);

  virtual ~BaseControlBlock() = default;
}

template <typename Deleter>
struct ControlBlock: BaseControlBlock {
  Deleter deleter;

  ControlBlock(const T&, size_t, size_t, const Deleter& deleter);
}
```

В самом shared_ptr мы будем хранить указатель на BaseControlBlock.

Какие проблемы в реализации:

1. Не поддержали кастомный аллокатор
2. Сейчас мы всегда храним T, а иногда нам нужно хранить лишь аллокатор на T
3. Приколы с указателями на Derived 

Это решается еще одним контрольным блоком: итого у нас их будет 3. 

### 3. Идейная реализация std::function

```c++
template <typename... Types>
struct function;

template <typename Res, typename... Args>
struct function<Res(Args...)> {
  struct Base {
    virtual Base* get_copy() = 0;
    virtual void destroy() = 0;
    virtual Res operator()(Args&&...) const = 0;
    virtual ~Base() = default;
  };

  template <typename Functor>
  struct Derived: public Base {
    Functor f;

    Derived(const Functor& f): f() {}

    Base* get_copy() override {
      return new Derived<Functor>(f);
    }

    void destroy() override {
      f.~functor();
    }

    Res operator()(Args&&... args) const override {
      return std::invoke(f, std::forward<Args>(args)...); // тут std::invoke из-за того что это может быть указателем на член
    }
  }
}
```

Есть важный нюанс: если мы конструируем наш function от сишной функции, то нет смысла заводить целый объект под это так как указатель на сишную функцию будет весить меньше. Поэтому в std::function используется SOO (small object optmization) и он вместо Base* будет хранить указатель на функцию.

Про подробное устройство std::function можно посмотреть [лекцию Ильи](https://www.youtube.com/watch?v=APtaCUYm03A&list=PL4_hYwCyhAvaWsj3-0gLH_yEZfKdTife0&index=11) для продвинутого потока



