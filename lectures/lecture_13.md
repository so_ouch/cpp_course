# Лекция 13

## 9. Стандартные контейнеры

### 1. Обзор контейнеров

Контейнеры делятся на две большие группы: последовательные и ассоциативные.

В данной лекции рассмотрим какие операции есть у каждого контейнера и за какое время эти операции работают.

#### 1. Последовательные

##### 1. std::vector

**Доступ к элементам:** 

- at/index\[\]

Возращает элемент по индексу. 

Работает за O(1)

- front/back

Возвращает первый/последний элемент. 

Работает за О(1)

**Модифицирующие операции:**

- insert/emplace по итератору

Работает за O(n)

- erase по итератору

Работает за O(n)

- push_back/emplace_back

Работает амортизированно за O(1)

- pop_back

Работает за O(1)

**Категория итератора**

ContiguousIterator

##### 2. std::deque

**Доступ к элементам:** 

- at/index\[\]

Возращает элемент по индексу. 

Работает за O(1)

- front/back

Возвращает первый/последний элемент. 

Работает за О(1)

**Модифицирующие операции:**

- insert/emplace по итератору

Работает за O(n)

- erase по итератору

Работает за O(n)

- push_back/emplace_back/push_front/emplace_front

Работает амортизированно за O(1)

- pop_back/pop_front

Работает за O(1)

**Категория итератора**

RandomAccessIterator

##### 3. std::list (forward_list)

**Доступ к элементам:**

- at/index\[\]

Отсутствует

- front/back(нет в forward_list)

Возвращает первый/последний элемент. 

Работает за О(1)

**Модифицирующие операции:**

- insert/emplace по итератору

Работает за O(1)

- erase по итератору

Работает за O(1)

- push_back(нет в forward_list)/push_front

Работает за O(1)

- pop_back(нет в forward_list)/pop_front

Работает за O(1)

**Категория итератора**

В std::list BidirectionalIterator

В std::forward_list ForwardIterator
    
#### 2. Ассоциативные

##### 1. std::map

Это отсортированный (по ключу) ассоциативный контейнер который хранит пары ключ-значение. Внутри чаще всего используется красно-черное дерево.


**Доступ к элементам**

- at/index[]

Работает за O(logn)

- find 

Работает за O(logn)

**Модифицирующие операции:**

- insert/emplace/erase

Работает за O(logn)

**Категория итератора**

BidirectionalIterator

##### 2. std::set

Это отсортированный ассоциативный контейнер. Внутри чаще всего используется красно-черное дерево.


**Доступ к элементам**

- at/index[]

Работает за O(logn)

- find 

Работает за O(logn)

**Модифицирующие операции:**

- insert/emplace/erase

Работает за O(logn)

**Категория итератора**

BidirectionalIterator

##### 3. std::unordered_map

Ассоциативный контейнер который хранит пары ключ-значение. Представляет собой хэш-таблицу

**Доступ к элементам**

- at/index[]

Работает за O(1) (не совсем честно)

- find 

Работает за O(1) (не совсем честно)

**Модифицирующие операции:**

- insert/emplace/erase

Работает за O(1) (не совсем честно)

**Категория итератора**

ForwardIterator

***Note*** "Не совсем честно" = зависит от наполненности хэш-таблицы и хэш-функции. 

##### 4. std::unordered_set

Ассоциативный контейнер. Представляет собой хэш-таблицу

**Доступ к элементам**

- at/index[]

Работает за O(1) (не совсем честно)

- find 

Работает за O(1) (не совсем честно)

**Модифицирующие операции:**

- insert/emplace/erase

Работает за O(1) (не совсем честно)

**Категория итератора**

ForwardIterator

***Note*** "Не совсем честно" = зависит от наполненности хэш-таблицы и хэш-функции. 


#### 3. Адаптеры

В stl существуют так же адаптеры над контейнерами: stack, queue, priority_queue. 

Работают они довольно просто: используют методы контейнера который хранят в полях. Например std::stack использует std::deque по умолчанию. 

### 3. Внутреннее устройство контейнеров

Давайте посмотрим как устроен код стандартных контейнеров. Особое внимание уделим разбору std::vector, так как это самый простой контейнер в stl. 

#### 1. Внутреннее устройство std::vector

Быстро напишем простейшую заготовку вектора

```c++
template <typename T>
class Vector {
 public:
  Vector(size_t n, const T& value = T());

  T& operator[](size_t index) {
    return arr_[index];
  }

  const T& operator[](size_t index) const {
    return arr_[index];
  }

  T& at(size_t index) {
    if (index >= size_) {
      throw std::out_of_range("...");
    }

    return arr_[index];
  }

  const T& at(size_t index) const {
    if (index >= size_) {
      throw std::out_of_range("...");
    }

    return arr_[index];
  }

  size_t size() const {return size_; }
  size_t capacity() const {return capacity_; }
 private:
  T* arr_ = nullptr;
  size_t capacity_ = 0;
  size_t size_ = 0;

}
```

Теперь допишем к вектору методы resize и reserve. Чтобы лучше прочувствовать разницу между ними давайте посмотрим на несколько примеров:

```c++
std::vector<int> v;
for (int i = 0; i < 50; ++i) {
  std::cout << v.size() << ' ' << v.capacity() << std::endl;
}
```

Заметим что capacity и size различаются. Мы уже знаем что динамический массив (каковым вектор и является) расширяет свою память в два раза при переполнении. 

Еще пример:


```c++
std::vector<int> v;
for (int i = 0; i < 50; ++i) {
  v.pop_back()
  std::cout << v.size() << ' ' << v.capacity() << std::endl;
}
```

Capacity не уменьшается (даже если вызовем clear). Это потому что вектор не очищает освободившуюся память. Для этого в c++11 появился метод shrink_to_fit

```c++
std::vector<int> v;
for (int i = 0; i < 50; ++i) {
  v.pop_back()
  v.shrink_to_fit();
  std::cout << v.size() << ' ' << v.capacity() << std::endl;
}
```

Продолжим писать вектор. Допишем метод reserve

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = new T[n];
  for (size_t i = 0; i < size_; ++i) {
    new_arr[i] = arr[i];
  }

  delete[] arr;
  arr = new_arr;
}
```

В этом коде есть следующие проблемы:

1. T* new_arr = new T[n]: может не быть конструктора по умолчанию
2. Мы зачем-то создаем объекты, хотя нас об этом не просили

Давайте исправлять

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);
  for (size_t i = 0; i < size_; ++i) {
    new_arr[i] = arr[i];
  }

  delete[] arr;
  arr = new_arr;
}
```

Уже лучше! Но теперь в строке  new_arr[i] = arr[i]; у нас сегфолт :(

Нам нужно вызвать конструктор T на памяти arr[i]. Как это сделать?

Нам поможет оператор emplacement new

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);
  for (size_t i = 0; i < size_; ++i) {
    new(new_arr + i) T(arr[i]); // тут воспользовались emplacement new
  }

  delete[] arr;
  arr = new_arr;
  capacity_ = n;
}
```

Теперь у нас сегфолт в строке delete[] arr. Исправляем

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);
  for (size_t i = 0; i < size_; ++i) {
    new(new_arr + i) T(arr[i]);
  }

  for (size_t i = 0; i < size_; ++i) {
    (arr + i)->~T();
  }

  delete[] reinterpret_cast<int8_t*>(arr);
  arr = new_arr;

  capacity_ = n;
}
```

Этот код все еще не идеален :( Проблемы следующие:

1. Копирование это не очень эффективно
2. Небезопасно относительно исключений (если конструктор копирования бросит исключение, то мы не освободим нашу память)

Первую проблему мы сможем исправить только когда пройдем move-семантику. Поэтому давайте исправим вторую:

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);

  size_t i = 0;
  try {
    for (i < size_; ++i) {
      new(new_arr + i) T(arr[i]);
    }
  } catch (...) {
    for (size_t j = 0; j < i; ++j) {
      (new_arr + j)->~T();
    }
    delete[] reinterpret_cast<int8_t*>(new_arr);
    throw;
  }
  

  for (size_t i = 0; i < size_; ++i) {
    (arr + i)->~T();
  }

  delete[] reinterpret_cast<int8_t*>(arr);
  arr = new_arr;

  capacity_ = n;
}
```

Чтобы не писать этот блок каждый раз, в стандартной библиотеке существует функция [std::uninitialized_copy](https://en.cppreference.com/w/cpp/memory/uninitialized_copy)

```c++
template <typename T>
void Vector<T>::reserve(size_t n) {
  if (n <= capacity_) {
    return;
  }

  T* new_arr = reinterpret_cast<T*>(new int8_t[n * sizeof(T)]);

  try {
    std::uninitialized_copy(arr_, arr_ + size_, new_arr);
  } catch (...) {
    delete[] reinterpret_cast<int8_t*>(new_arr);
    throw;
  }

  for (size_t i = 0; i < size_; ++i) {
    (arr + i)->~T();
  }

  delete[] reinterpret_cast<int8_t*>(arr);
  arr = new_arr;

  capacity_ = n;
}
```

Теперь давайте напишем push_back

```c++
template <typename T>
void Vector<T>::push_back(const T& value) {
  if (size_ == capacity_) {
    reserve(2 * capacity_);
  }

  new(arr + sz) T(value);
  size_ += 1;
} 
```

Ну можно и pop_back написать:

```c++
template <typename T>
void Vector<T>::pop_back(const T& value) {
  (arr + size_ - 1)->~T();
  size_ -= 1;
} 
```


resize пишется очевидно)

#### 2. Внутреннее устройство std::vector<bool\>

Хотелось бы чтобы вектор из булов тратил по биту на каждый бул. 

Если же использовать обычный вектор (как мы написали выше), то на каждый бул будет расходываться по байту. 

Для этого в stl есть специализация шаблона std::vector для bool. 

Посмотрим на следующий код: 


```c++
template <typename T>
void f() = delete;

std::vector<bool> v(10, false);
v[5] = true;
f(v[5]);
```

И мы видим что на самом деле v\[i\] это std::_Bit_reference.

Давайте посмотрим на примерную реализацию:

```c++
template <>
class Vector<bool> {
 public:
  struct BitReference {
    int8_t* cell;
    uint8_t index;

    BitReference& operator=(bool b) {
      if (b) {
        *cell |= (1u << num);
      } else {
        *cell &= ~(1u << num);
      }
    }

    operator bool() const {
      return *cell & (1u << num);
    }
  }

  BitReference operator[](size_t i) {
      return BitReference(arr_ + i / 8, i % 8);
  }
 private:
  int8_t* arr;
  size_t size_;
  size_t capacity_;
};
```

#### 3. Внутреннее устройство адаптеров на примере std::stack

Все адаптеры устроены +- одинаково. В полях они хранят нужный контейнер а в методах просто обращаются к нужным методам этого контейнера. 

```c++
template <typename T, typename Container = std::deque<T>>
class Stack {
 public:
  void push(const T& value) {
    container_.push_back(value);
  }

  void pop() {
    container_.pop_back();
  }

  T& top() {
    return container_.back();
  }
 private:
  Container container_;
}
```

<details> 
  <summary>Почему pop ничего не возвращает?</summary>
   Ответ: все ради эффективности, если бы pop что-то возвращал, то он был бы обязан копировать, а это проигрыш
</details>

Аналогично устроены queue и priority_queue

#### 4. Внутреннее устройство std::list

list как известно представляет собой связный список нод.

```c++
template <typename T>
class List {
 public:
  

 private:
  struct Node {
    T value;
    Node* prev;
    Node* next;
  }

  Node* head;
}
```

В односвязном списке (std::forward_list) указатель на prev отсутствует.

Интересные факты:

1. std::sort не работает для list, так как не RA итератор. Поэтому у листа есть встроенный метод sort (использует merge)
2. Есть метод merge
3. Есть метод сплайс


#### 5. Внутреннее устройство std::map

Хранит пары ключ, значение. При этом ключи упорядочены 

Итераторы константные

Реализация почти везде сделана на красно-черном дереве

Формат ноды:

```c++
struct Node {
  std::pair<const Key, Value> data;
  Node* prev;
  Node* left;
  Node* right;
}
```

***Note*** оператор [] создает элемент в мапке, если его не было (если не хотим создавать, используем at)

Обычно в std::map используется красно-черное дерево, которое изучается в курсе алгоритмов. 

#### 6. Внутреннее устройство std::unordered_map

Представляет собой хэш-таблицу. Подробней в курсе алгоритмов) 

### 4. Инвалидация итераторов

Давайте рассмотрим проблему на примере вектора. 

```c++
std::vector<int> v;
v.push_back(1);

std::vector<int>::iterator = v.begin();
int* first_ptr = &v[0];
int& first_ref = v[0];

for (int i = 0; i < 100; ++i) {
  v.push_back(i);
}
```

После такого цикла вектор поменяет свой размер и сделает реаллокацию. Тогда обращение по итератору/указателю/ссылке будет UB. Говорят что push_back инвалидирует итераторы/указатели/ссылки.

Давайте поймем какие операции над стандартыми контейнерами что делают.

![invalidation_rules](/lectures/images/invalidation_rules.png)