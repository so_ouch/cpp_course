# Лекция 8


## 4. Шаблоны

### 1. Основная идея и синтаксис

Шаблонными могут быть не только функции, но и классы. Синтаксис такой же:


```c++
template <typename T>
class vector {
    ...
};
```

Еще бывают шаблонные псевдонимы (since c++11):

```c++
template <typename T>
using MyMap = std::map<T, T>;

MyMap<int> map; // синоним для std::map<int, int> map;
```

А так же бывают шаблонные константы (since c++14):

```c++
template <typename T>
const T pi = 3.14;
```

А еще бывают шаблонные методы:

```c++
struct Vector {
    template <typename U>
    void push_back(const U&);
}
```

Причем шаблонный метод можно сделать даже внутри уже шаблонного класса


```c++
template <typename T>
struct Vector {
    template <typename U>
    void push_back(const U&);

    T value;
}
```

Тогда, чтобы определить такой метод вне класса нужно написать

```c++
template <typename T>
struct Vector {
    template <typename U>
    void push_back(const U&);

    T value;
}

template <typename T>
template <typename U>
void Vector<T>::push_back(const U&) {
    ...
}
```

***Note*** template <typename T, U> void Vector<T>::push_back(const U&) не сработало бы!

***Note*** типы могут быть не только T и U, просто это общепринятые названия. Можно (но не нужно) писать даже так:


```c++
template <typename CoolTemplate>
struct Vector {
    template <typename AnotherCoolTemplate>
    void push_back(const AnotherCoolTemplate&);

    T value;
}

template <typename NotCoolTemplate>
template <typename AnotherNotCoolTemplate>
void Vector<NotCoolTemplate>::push_back(const AnotherNotCoolTemplate&) {
    ...
}
```

Еще вместо typename можно писать class, например так:

```c++
template <class T>
T GetMax(T a, T b);
```

и ничего не поменяется :) typename и class в контексте шаблонов синонимы!

### 2. Работа шаблонов с точки зрения компилятора 

На самом деле на этапе препроцессинга (первый этап компиляции) генерируется код для всех шаблонных подстановок. Например:

Пусть есть функция 

```c++
template <typename T>
T GetMax(const T& a, const T& b) {
    return a > b ? a : b;
}
```

Если она вообще не вызывалась, то ее и не будет в итоговом программном коде.

Если она вызывалась от int, то сгенерируется ее версия для int и тд



### 3. Перегрузка шаблонных функций

Рассмотрим следующий код:

```c++
#include <iostream>

template <typename T>
void f(T x) {
    std::cout << 1;
}

void f(int x) {
    std::cout << 2;
}

int main() {
    f(0);
}
```

<details> 
  <summary>Какая функция вызовется?</summary>
   Ответ: вторая
</details>

Так происходит потому что компилятор выбирает более частный случай.

***Note*** Это именно перегрузка шаблонных функций, а не специализация шаблона

Давайте усложним наш код

```c++
#include <iostream>

template <typename T, U>
void f(T x, U y) {
    std::cout << 1;
}

template <typename T>
void f(T x, T y) {
    std::cout << 2;
}

void f(int x, double y) {
    std::cout << 3;
}

int main() {
    f(0, 0);
}
```

<details> 
  <summary>Какая функция вызовется?</summary>
   Ответ: вторая
</details>

В данном примере 2я версия лучше 1й потому что более частная и лучше 3й потому что не надо делать приведение типов

Отсюда нам важно запомнить два основных правила перегрузки функций:

1. Частное лучше общего

2. Если есть более частная версия, но нужно будет сделать приведение типов, то лучше более общая версия

Формальные правила найдете в стандарте :) 

Еще один интересный пример:

```c++
#include <iostream>

template <typename T>
void f(T x) {
    std::cout << 1;
}

template <typename T>
void f(T& x) {
    std::cout << 2;
}

int main() {
    int x = 0;
    int& y = x;
    f(y);
}
```

<details> 
  <summary>Какая функция вызовется?</summary>
   Ответ: будет CE
</details>

Так происходит, потому что вызвать функцию от x и от y в данном контексте это одно и тоже. А для x уже будет ambiguous call

А теперь перепишем немного код

```c++
#include <iostream>

template <typename T>
void f(T& x) {
    std::cout << 1;
}

template <typename T>
void f(const T& x) {
    std::cout << 2;
}

int main() {
    int x = 0;
    int& y = x;
    f(y);
}
```

Теперь неоднозначности не будет, потому что к const int& надо еще кастить 

### 4. Специализация шаблонов

#### 1. Сначала про классы

Давайте представим, что мы реализуем вектор с шаблонным параметром T


```c++
template <typename T>
class Vector {
    ...
}
```

Вспомним, что вектор из bool можно реализовать по-другому (чтобы одному значению соответствовал 1 бит в идеале). То есть нам нужна специализация нашего вектора для bool.
Пишется это так:


```c++
template <>
class Vector<bool> {
    ...
}
```

Причем новый класс даже по методам не должен совпадать с исходным (вспомним как реализуются шаблоны и поймем почему)

Это называется полной шаблонной специализацией. Существует так же частичная специализация:

```c++
template <typename T>
class Vector<T*> {
    ...
}
```

Эта специализация будет работать для всех типов T, которые являются указателями.

Можно еще специализировать не все шаблонные аргументы:

```c++
#include <iostream>

template <typename T, typename U>
class C {
    ...
}

template <typename T, int>
class C {
    ...
}
```

Выбор специализации работает по тем же условным правилам, что и выбор перегрузки

#### 2. Теперь про функции

Так как у функций существует механизм перегрузки, то понятие частичной специализации к ним не применимо. Посмотрим на следующий код (номер функции = то что выводим в поток):

```c++
#include <iostream>

template <typename T, typename U>
void f(T, U) {
    std::cout << 1;
}

template <typename T>
void f(T, T) {
    std::cout << 2;
}

template <>
void f(int, int) {
    std::cout << 3;
}

int main() {
    f(0, 0);
}
```

<details> 
  <summary>Какая функция вызовется?</summary>
   Ответ: вызовется 3 версия
</details>

Однако, если мы сделаем небольшую перестановку:

```c++
#include <iostream>

template <typename T, typename U>
void f(T, U) {
    std::cout << 1;
}

template <>
void f(int, int) {
    std::cout << 3;
}

template <typename T>
void f(T, T) {
    std::cout << 2;
}

int main() {
    f(0, 0);
}
```

то вызовется 2 версия!!!

А теперь попробуем разобраться почему так:

1. Специализация применилась к 1 функции
2. На моменте выбора перегрузки выиграла 2, так как она более частная
3. У 2 версии нет специализации, поэтому она вызвалась сама

То есть специализация выбирается уже после выбора перегрузки!

А теперь сделаем вот так:

```c++
#include <iostream>

template <typename T, typename U>
void f(T, U) {
    std::cout << 1;
}

void f(int, int) {
    std::cout << 3;
}

template <typename T>
void f(T, T) {
    std::cout << 2;
}

int main() {
    f(0, 0);
}
```

Теперь опять выберется 3 версия (потому что это уже не специализация, а перегрузка)


### 5. Шаблонные аргументы по умолчанию

Ну, они есть)))

```c++
template <typename T = int>
void f(T) {
    ...
}
```

Более явный пример: 

```c++
template <typename T, typename Cmp = std::less<T>>
void sort(...) {
    ...
}
```

### 6. Non-type template parameters 

#### 1. Числовые параметры

Рассмотрим следующий код: 

```c++
template <typename T, int N>
struct Array {
    T arr_[N];
};
```

N здесь это и есть шаблонный параметр, не являющийся типом. 


 
При этом следующий код:

```c++
Array<int, 5> arr1;
Array<int, 10> arr2;
arr2 = arr1;
```

работать не будет, потому что с точки зрения компилятора это разные типы


***Note*** шаблонными параметры, не являющимися типами могут быть только целочисленные типы а также bool и char 

Хороший пример класса, который использует такие шаблонные параметры это std::array (обертка над честным массивом).

Зачем это вообще нужно? Чтобы типы от разных констант не были совместимы друг с другом

А теперь пара интересных моментов. 

Посмотрим на следующий код


```c++
int d = 0;
std::cin >> d;
std::array<int, y> arr1; // 1 пример

int c = 10;
std::array<int, c> arr1; // 2 пример

const int y = c;
std::array<int, y> arr1; // 3 пример

const int x = 10;
std::array<int, x> arr1; // 4 пример
```

<details> 
  <summary>Какие примеры заработают</summary>
   Ответ: только 4
</details>


#### 2. Шаблонные параметры шаблонов

![WTF](/lectures/memes/wtf.jpg)

Допустим мы пишем свой Stack и хотим передавать в шаблонах какой контейнер использовать, то есть хотим пользоваться им примерно так:

```c++
template <...>
class Stack {
  public:
    ...
  private:
    Container<T> container_;
}

int main() {
    Stack<int, std::vector> s;
}
```

Чтобы это работало нам надо написать объявление нашего класса вот так:

```c++
template <typename T, template <typename U> typename Container>
class Stack {
  public:
    ...
  private:
    Container<T> container_;
}
```

На самом деле U можно не писать, так как этот тип нигде не используется. Тогда наш класс будет выглядеть вот так:


```c++
template <typename T, template <typename> typename Container>
class Stack {
  public:
    ...
  private:
    Container<T> container_;
}
```

### 7. Проблема зависимых имен

Посмотрим на следующий код

```c++
template <typename T>
struct S {
    using X = T;
};

template <>
struct S<int> {
    static int X; 
};

int a = 0;

template <typename T>
void f() {
    S<T>::X * a;
}

int main() {
    f<int>();
    f<double>();
}
```
Обратим внимание на строку S<T>::X * a
Эта строка может принимать разный синтаксический смысл в зависимости от T (либо declaration (указатель) либо expression (умножение)), поэтому такой код не скомпилируется (для разных типов это будет то выражение то объявление)

Собственно это и есть "проблема зависимых имен", здесь X зависит от T.

Есть следующее правило: если компилятор встречает зависимое имя и не может понять что это, то он по умолчанию считает что expression, поэтому если вызвать только f<int>() то все будет ок

Если нужно чтобы компилятор парсил зависимое имя как declaration, то нужно написать typename (это вторая роль слова typename в C++)


```c++
...

template <typename T>
void f() {
    typename S<T>::X * a;
}
```

Посмотрим на еще один интересный пример

```c++
template <typename T>
struct S {
    template <int M, int N>
    struct A{};
}

template <>
struct S<int> {
    static const int A = 0;
}

template <typename T>
void f() {
    S<T>::A<1,2> a;
}
```

Тогда в строке S\<T\>::A\<1,2\> a; опять возникнет неодназначность

1. Ее можно понять как объявление переменной a типа S\<T\>::A\<1,2\>

2. Ее можно понять как S\<T\>::A <(меньше) 1 , 2 >(больше) a

А теперь самое забавное:

Даже если мы добавим слово template код 

```c++
template <typename T>
struct S {
    template <int M, int N>
    struct A{};
}

template <>
struct S<int> {
    static const int A = 0;
}

template <typename T>
void f() {
    typename S<T>::A<1,2> a;
}
```

все равно не скомпилируется, потому что слово typename лишь означает что дальше будет название типа, а не шаблон. Чтобы это пофиксить нужно переписать функцию f вот так:

```c++
template <typename T>
void f() {
    typename S<T>::template A<1,2> a;
}
```

то есть у слова template тоже два значения в языке C++

Попробуйте придумать пример где слово typename не нужно, а слово template нужно

### 8. Basic type-traits

Сейчас мы научимся писать простейшие мета-функции (функции от типов)

#### 1. is_same

Мы хотим научиться писать какой-то такой код (причем ifы должны проверяться в compile-time):

```c++
template <typename U, typename V>
void f(U x, V y) {
    ...
    if (U == V) {
        ...
    }
}
```

Давайте напишем структуру is_same для этого!

```c++
template <typename U, typename V>
struct is_same {
    static const bool value = false;
};

template <typename U>
struct is_same<U,U> {
    static const bool value = true;
};
```

Тогда нашу заготовку можно переписать вот так:


```c++
template <typename U, typename V>
void f(U x, V y) {
    ...
    if (is_same<U,V>::value) {
        ...
    }
}
```

Задачка на размышление: поймите почему это работает) 

#### 2. remove_const

```c++
template <typename T>
struct remove_const {
    using type = T;
};

template <typename T>
struct remove_const<const T> {
    using type = T;
};
```

Зачем это вообще нужно?

![WHY](/lectures/memes/why.jpeg)

Допустим мы пишем функцию f(T value) и нам в нее передали константный объект. И мы внутри этой функции хотим объявить переменную типа T (не константную). Подумайте как это можно реализовать с помощью remove_const

<details> 
  <summary>Как <s>какать<s>?</summary>
   Ответ: typename remove_const &lt;T&gt;::type x; 
</details>

Собственно в C++11 появился заголовочный файл \<type_traits\> в котором лежат эти и многие другие мета-функции. Про сложные из них мы поговорим попозже)

На завтравочку: мы научимся писать функцию которая умеет считать количество полей у простых структур, а так же научимся принтить любую структуру

Упражнение: напишите мета-функцию, которая будет снимать с типа все дополнительные вещи (указатель, ссылки, константы) 