# Лекция 17

## 12. Вывод типов

### 4. Structured bindings (since c++17)

```c++
int main() {
  std::pair<int, std::string> p(5, "abc");
  auto [a, b] = p;
  std::cout << a << " " << b << std::endl;
}
```

В этом контексте на auto опять же таки можно навешивать украшатели типа

Это еще работает для std::tuple, работает для сишного массива, работает даже для data members!

Более глубокое понимание https://devblogs.microsoft.com/oldnewthing/20201015-00/?p=104369

## 13. Умные указатели

### 1. Введение

Умные указатели - указатели которые следят за освобождением ресурса. Построены на идиоме RAII. Вспомним первую проблему где нам они понадобились:

```c++
void f(int x) {
  int* p = new int(1000);
  if (x == 0) {
    throw 1;
  }

  delete p;
}
```

Хотелось бы, чтобы p в деструкторе сам очищал память.

Начнем с более простого умного указателя

### 2. unique_ptr

Unique_ptr - простейший умный указатель. Он запрещает себя копировать и удаляет ресурс в деструкторе. Простота именно в запрете на копирование (реализовать shared_ptr, где несколько объектов владеют ресурсом куда сложней)

Реализация unique_ptr супер простая, приводить ее здесь не будем. Важно реализовать noexcept move-конструктор и move-оператор присваивания, не забыть про стрелочку и звездочку

Еще у умных указателей есть метод .get() который возвращает сырой указатель

Еще из приколов: вот такой код не будет нормально работать

```c++
std::unique_ptr<int> p(new int[5]);
```

Потому что в деструкторе будет вызван delete p. Для массивов существует отдельная специализация 

```c++
std::uniqe_ptr<int[]>p(new int[5]);
```

***Note*** Начиная с c++17 для специализации с массивами есть квадратные скобки

### 3. shared_ptr

Тут много букав про то как пользоваться shared_ptr. Приводить эти букавы не будем :) 

Давайте попробуем лучше накидать реализацию

```c++
template <typename T>
class shared_ptr {
 public:
  explicit shared_ptr(T* ptr): ptr(ptr) {}
 private:
  T* ptr;
};
```

Заготовка есть. Давайте разбираться со счетчиком (shared_ptr должен понимать количество владельцев объектом)

Первая версия:

```c++
template <typename T>
class shared_ptr {
 public:
  explicit shared_ptr(T* ptr): ptr(ptr), count(1) {}
 private:
  T* ptr;
  size_t count;
};
```
Это очевидно работать не будет, например при копировании нам нужно обновить счетчик у всех инстансов

Следующий вариант:

```c++
template <typename T>
class shared_ptr {
 public:
  explicit shared_ptr(T* ptr): ptr(ptr), count(1) {}
 private:
  T* ptr;
  static size_t count;
};
```

Теперь у нас счетчик общий вообще для всего класса shared_ptr\<T\>. Не подойдет!

Лучшее решение:

```c++
template <typename T>
class shared_ptr {
 public:
  explicit shared_ptr(T* ptr): ptr(ptr), count(new int(1)) {}
 private:
  T* ptr;
  size_t* count;
};
```

***Note*** невероятно душное замечание: конечно же нам нужно уметь принимать кастомный аллокатор для выделения этого счетчика. Кстати shared_ptr еще умеет принимать кастомный Deleter для удаления ресурса в деструкторе

Давайте попробуем написать деструктор

```c++
template <typename T>
class shared_ptr {
 public:
  explicit shared_ptr(T* ptr): ptr(ptr), count(new int(1)) {}
  ~shared_ptr() {
    if (count == nullptr) {
      return;
    }

    --*count;

    if (*count == 0) {
      delete ptr;
      delete count;
    }
  }
 private:
  T* ptr = nullptr;
  size_t* count = nullptr;
};
```

Про остальные методы shared_ptr проще почитать на цппреф

### 4. std::make_shared/unique

#### Общая идея

Давайте посмотрим на следующий код:

```c++
int main() {
  int* p = new int(5);
  std::shared_ptr<int> sp(p);
}
```

Так делать не надо. Ну то есть так конечно все будет работать, но это позволяет вам сделать вот так:


```c++
int main() {
  int* p = new int(5);
  std::shared_ptr<int> sp1(p);
  std::shared_ptr<int> sp2(p);
}
```
А это уже UB.

Вот такой код тоже лучше не писать (ничего плохого в нем нет, но кодстайл так себе):

```c++
int main() {
  std::shared_ptr<int> sp(new int(5));
}
```

Интересный пример:

```c++
int g(const std::shared_ptr<int>& p, int x) {}

int f(int x) {
  if (x == 0) {
    throw 1;
  }

  return x + 1;
}

int main() {
  g(shared_ptr<int>(new int(10)), f(0));
}
```

Теоритически компилятор может сначала сделать new int(10) а потом f(0) (до c++17)

Стл предоставляет интерфейсы для создания умных указателей без обращения к new

#### Реализация std::make_unique

```c++
template <typename T, typename... Args>
uniqe_ptr<T> make_unique(Args&&... args) {
  return uniqe_ptr<T>(new T(std::forward<Args>(args)...));
}

int main() {
  auto p = std::make_unique<int> p(5);
}
```

#### Реализация std::make_shared

Тут все интересней. Зачем нам отдельно делать new для T и new для счетчика, если можно сделать один new. Давайте немного изменим наш класс shared_ptr

```c++
template <typename T>
class shared_ptr {
 public:
  explicit shared_ptr(T* ptr): ptr(ptr), count(new int(1)) {}
  ~shared_ptr() {
    if (control_block_ == nullptr) {
      return;
    }

    --control_block_->count;

    if (control_block_->count == 0) {
      delete control_block_;
    }
  }
 private:
  template <typename U>
  struct ControlBlock {
    U object;
    size_t count;
  }

  template <typename U, typename... Args>
  friend shared_ptr<U> make_shared(Args&&... args);

  shared_ptr(ControlBlock<T>* ptr): control_block_(control_block_) {}

  ControlBlock<T>* control_block_;
};

template <typename T, typename... Args>
shared_ptr<T> make_shared(Args&&... args) {
  auto p = new ControlBlock(1, std::forward<Args>(args)...);
  return shared_ptr<T>(p);
}
```

### 5. weak_ptr

Давайте посмотрим на следующий код:

```c++
struct S {
  std::shared_ptr<S> ptr;
}

int main() {
  auto s1 = std::make_shared<S>();
  auto s2 = std::make_shared<S>();

  s1.ptr = s2;
  s2.ptr = s1;
}
```

В данном случае получится утечка памяти, потому что shared никогда не удалится. Данная проблема может показаться надуманной, однако возникает очень много ситуаций в реальном мире где эта проблема актуальна (например хранение родителя в дереве, хранение указателя на предыдущий элемент в списке). Из-за этой проблемы сборка мусора это не очень тривиальная задача. Одно из решений этой проблемы это "слабые ссылки". В случае c++ это weak_ptr

Weak ptr хоть и назван как указатель, однако он не ведет себя как таковой. Его нельзя разименовывать! 

Weak ptr умеет делать только две вещи:

1. Отвечать на вопрос "Сколько еще шаредов указывает на объект"

2. По запросу создавать новый шаред на объект


Т.е. weak ptr не владеет объектом и не учавствует в подсчете ссылок.


Такая конструкция решает наши проблемы: просто используем weak_ptr для указания на родителей

Давайте накидаем скелет реализации

```c++
template <typename T>
class weak_ptr {
 public:
  weak_ptr(const shared_ptr<T>& ptr): helper_(ptr.helper_) {}

  bool expired() const {}

  shared_ptr<T> lock() const { return shared_ptr<T>(helper_); }
 private:
  ControlBlock<T>* helper_ = nullptr;
}
```

Вспомним, что наш контрольный блок выглядит так: 

```c++
template <typename U>
struct ControlBlock {
  T object;
  size_t count;
}
```

Очень хочется написать метод expired вот так:

```c++
template <typename T>
class weak_ptr {
 public:
  weak_ptr(const shared_ptr<T>& ptr): helper_(ptr.helper_) {}

  bool expired() const {
    return helper_->count == 0;
  }

  shared_ptr<T> lock() const { return shared_ptr<T>(helper_); }

 private:
  ControlBlock<T>* helper_ = nullptr;
}
```

На самом деле это будет ошибка, потому что при обнулении счетчика наш control_block удаляется в деструкторе shared_ptr

На самом деле нам нужно завести второй счетчик, а так же хранить объект внутри блока через указатель 

```c++
template <typename U>
struct ControlBlock {
  T* object;
  size_t shared_count;
  size_t weak_count;
}
```

Теперь в деструкторе shared_ptr нам нужно удалять объект при обнулении shared_count и удалять весь блок при обнулении weak_count

в weak_ptr в методе expired мы можем смотреть на shared_count, а в деструкторе удалять блок если weak_count становится нулем