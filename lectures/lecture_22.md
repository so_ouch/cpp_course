# Лекция 22

Благодарности курсу Владимирова Константина. [Ссылка на его лекцию](https://www.youtube.com/watch?v=HxdrGM6ZbNs)

## 16. Концепты

### 1. Идея

Давайте посмотрим на пример обобщенного кода функции contains

```c++
template <typename R, typename T>
bool contains(const R& range, const T& value) {
  for (const auto& x : range) {
    if (x == value) {
      return true;
    }
  }

  return false;
}
```

Этот код неявно накладывает некоторые требования: 

1. range должен предоставлять begin и end причем возвращаемое значение можно инкрементировать и разименовывать
2. Должен быть определен оператор== для T и того что возвращает ```range.begin();```

Эти "требования" накладывает сам компилятор: мы просто не скомпилируем наш код если их не выполним:

```c++
std::vector<std::string> v = {"a", "b", "c"};
bool v = contains(v, 1);
```

При этом компилятор выдает жуткое полотно ошибки про шаблонные подстановки, отсутствие функции и тд. 

Как же нам решить эту проблему? Давайте пользоваться теми инструментами которые мы уже изучили. 

Первый путь: написать интерфейсы:

```c++
bool contains(const IRange* range, const IVal* value) {
  for (auto it = range->begin(); it != range->end(); it = it->next()) {
    if (it->get()->compare(value)) {
      return true;
    }
  }

  return false;
}
```

Сами классы интерфейсов придумываются достаточно просто. Это конечно порождает очевидную проблему: придется каждый класс который мы хотим передать в contains наследовать от наших интерфейсов :(

Давайте сделаем явные ограничения на типы при помощи SFINAE. Упростим наш пример и будем писать сначала для простой функции: 

```c++
template <typename T, typename U>
bool CheckEq(T&& lhs, U&& rhs) {
  return lhs == rhs;
}
```

Нам нужно написать проверку того, что типы T и U могут сравниваться

```c++
template <typename T, typename U, typename = void>
struct is_equality_comparable: std::false_type {};

template <typename T, typename U>
struct is_equality_comparable<T, U, std::void_t<decltype(std::declval<T>() == std::declval<U>())>>: std::true_type {};
```

***Note*** [std::void_t](https://en.cppreference.com/w/cpp/types/void_t) маппит все типы внутри в void. Полезная штука для SFINAE приколов

***Note*** эту штуку достаточно просто сломать ссылками и константами. Но не будем об этом

У нас есть три варианта как впихнуть эту проверку в функцию:

1. Испортить один шаблонный аргумент (на самом деле тоже не очень, но лучший из трех)
2. Испортить возвращаемый тип (плохо)
3. Испортить параметр функции (плохо)

Давайте выберем первый способ 

```c++
template <typename T, typename U, typename = std::enable_if_t<is_equality_comparable<T, U>::value>>
bool CheckEq(T&& lhs, U&& rhs) {
  return lhs == rhs;
}
```

Теперь у нас будет сообщение об ошибки типа: не можем найти функцию.

Давайте перечислим все проблемы

1. Можем просто явно указать шаблонный аргумент
2. На каждое ограничение делать новый шаблонный аргумент

Какие у нас другие механизмы? static_assert и if constexpr

```c++
template <typename T, typename U>
bool CheckEq(T&& lhs, U&& rhs) {
  if constexpr (!is_equality_comparable<T, U>::value) {
    static_assert(false, "AAAAA");
  } else {
    return lhs == rhs;
  }
}
```

Проблемы тут: 

1. Нужна новая ветка ифа на каждое ограничение 
2. Можем сравнивать не через == а через < соответственно опять получим кучу веток ифов 

Мы бы хотели чтобы просто была перегрузка на == и на <. Но мы не можем делать перегрузку по шаблонному параметру! 

Языку явно был необходим механизм явных контрактов, и его дали нам! 

### 2. requires

Давайте перепишем наш пример по-нормальному:

```c++
template <typename T, typename U> requires is_equality_comparable<T, U>::value 
bool CheckEq(T&& lhs, U&& rhs) {
  return lhs == rhs;
}
```

В requires можно передать любоею constexpr выражение.

Стало сильно лучше! Теперь нам очень просто объединять ограничения: 

```c++
tempate <typename Iter>
requires (std::is_forward_iterator_v<Iter> && std::is_totally_ordered_t<typename Iter::value_type>)
Iter MinElement(Iter first, Iter second) {...}
```

При этом компилятор будет нам говорить какое именно условие нарушено. 

А еще по requires можно делать перегрузки! 

```c++
struct S {
  template <typename Int>
  requires std::is_integral_v<Int>
  S(Int x) {...}

  requires std::is_floating_point_v<Float>
  template <typename Float>
  S(Float x) {...}
}
```

Перегрузка по requires работает потому что requires входит в разрешение имен.

Проверяется это с точностью до лексем. То есть такие функции считаются разными:

```c++
template <typename T>
requires (1 == 1)
void f(T x) {}

template <typename T>
requires true
void f(T x) {}
```

Навешивание requires на non-templated функции запрещено. 

***Note*** non-templated - нигде нет шаблона. Можно навешивать requires на методы шаблонного класса

У нас все еще есть проблемы с нашими SFINAE-consraints:

```c++
template <typename Iter>
requires is_input_iterator_t<Iter>
int distance(Iter first, Iter last) {
  int result = 0;
  while (first != last) {
    ++first;
    ++result;
  }

  return result;
}

template <typename Iter>
requires is_random_iterator<Iter>
int distance(Iter first, Iter last) {
  return last - first;
}
```

Для RA итератора будут подходить обе функции, потому что наши ограничения не знают что такое частное-общее.

А еще SFINAE-constaints неприятно писать :( Пример: 

```c++
template <typename T, typename = void>
struct is_totally_ordered: std::false_type {};

template <typename T>
struct is_totally_ordered<T, std::void_t<
  decltype(std::declval<T>() == std::declval<T>()),
  decltype(std::declval<T>() <= std::declval<T>()),
  decltype(std::declval<T>() < std::declval<T>())
>> : std::true_type {};
```

Поэтому нам нужен новый функционал!

### 3. Сложные ограничения

Давайте перепишем наш CheckEq:

```c++
template <typename T, typename U> 
requires requires(T t, U u) {t == u; }
bool CheckEq(T&& lhs, U&& rhs) {
  return lhs == rhs;
}
```

Да-да requires-requires :)

requires expression это constexpr предикат над SFINAE характеристиками. 
Он возвращает true если то, что написано внутри него, возможно.

То есть в нашем случае если ```t==u``` возможно, то будет true.

***Note*** Никаких вычислений внутри не происходит! 

Есть 4 основных формы того что может быть внутри requires:

1. simple requirement
2. type requirement
3. compound requirement
4. nested requirement

```c++
requires(T t, U u) {
  u + b; // true если u+v возможно (simple)
  typename T::iner; // true если T::inner есть (type)
}
```

Про compound и nested попозже.

Следующая проблема: не хочется каждый раз писать одинаковый requires если они повторяются. Поэтому нам нужны концепты!

### 4. Концепты

Это языковая возможность сочитать разного вида ограничения и запихнуть их в одну сущность:

```c++
template <typename From, typename To>
concept convertible_to = 
  std::is_convertible_v<From, To> &&
  requires {
    static_cast<To>(std::declval<From>()); 
  };

template <typename T> 
requires convertible_to<T, int>
int foo() {...}
```

### 5. Составные ограничения 

Составные ограничения проверяют совместимость типов с выражениями

```c++
requires requires(T x) {
  {*x} -> std::convertible_to<typename T::inner>;
}
```

Это будет true если: 

1. *x валидно
2. T::inner валидно
3. *x можно сконвиртировать в T::inner

После стрелочки должен идти type-constraint. 

Обратите внимание: концепт convertible_to принимает два аргумента, а мы явно указали только 1. Это потому что аргумент автоматически подставится

Есть также отдельный синтаксис для noexcept:

```c++
requires requires(T x) {
  {++x} noexcept;
}
```

### 6. Вложенные ограничения 

Внутрь requires можно засунуть еще requires) 

Пример с cppref:

```c++
template<class T>
concept Semiregular = DefaultConstructible<T> &&
    CopyConstructible<T> && CopyAssignable<T> && Destructible<T> &&
requires(T a, std::size_t n)
{  
    requires Same<T*, decltype(&a)>; // nested: "Same<...> evaluates to true"
    { a.~T() } noexcept; // compound: "a.~T()" is a valid expression that doesn't throw
    requires Same<T*, decltype(new T)>; // nested: "Same<...> evaluates to true"
    requires Same<T*, decltype(new T[n])>; // nested
    { delete new T }; // compound
    { delete new T[n] }; // compound
};
```

requires внутри requires это единственный случай когда внутри requires что-то вычисляется! 

### 7. Концепты из концептов

Давайте накидаем простой концепт: типы можно сравнивать

```c++
template <typename T, typename U>
concept ComparableWith = requires(T t, U u) {
  {t == u} -> convertible_to<bool>;
  {t != u} -> convertible_to<bool>;
  {u == t} -> convertible_to<bool>;
  {u != t} -> convertible_to<bool>;
}
```

И теперь мы можем завести концепт на сравнимость с самим собой: 

```c++
template <typename T>
concept Comparable = ComparableWith<T, T>;
```

На сами концепты вешать ограничения нельзя))

А еще концепт считается объявленным только после полного выражения. То есть внутри определения концепта нельзя вызывать его самого. То есть нет рекурсивного определения) 

### 8. Сокращенный синтаксис

Базово мы пишем так: 

```c++
template <typename T> requires AmazingConcept<T> void foo(T&);
```

А можно писать вот так: 

```c++
template <AmazingConcept T> void foo(T&);
```

Если у вашего концепта несколько шаблонных аргументов, то можно делать так: 

```c++
template <AmazingConcept<int, double> T> void foo(T&);
```

### 9. Вложение концептов

Концепты позволяют нам справится с нашей проблемой "частное и общее"

```c++
template <typename T> concept Ord = requires(T a, T b) { a < b; };
template <typename T> concept Inc = requires(T a) { ++a; };
template <typename T> concept Void = std::is_same_v<T, void>;

template <typename T> requires Ord<T> || Inc<T> || Void<T>
struct less {
  int operator()() const {return 1; }
}

template <Ord T> 
struct less<T> {
  int operator()() const {return 2; }
}

template <>
struct less<void> {
  int operator()() const {return 3; }
}
```

Вычисления в концптах ленивые как и в обычных бул операциях. 

В стандарте есть определение отношения subsumes на концептах (вложение): 

***Def*** Констрейнт P включает в себя констрейнт Q если и только если для каждого дизъюнктивного условия Pi в дизъюнктивной нормальной форме P Pi включает в себя каждое конъюктивное условие Qj в нормальной конъюктивной форме Q

```c++
template <typename T>
concept P = Q<T> && R<T>; 
```

P включает в себя Q и R;



