# Лекция 21

## 15. Шаблонное метапрограммирование, SFINAE и вычисления в compile-time

### 1. Идея SFINAE и простейший пример

SFINAE = substitution failure is not an error

"Неудачная шаблонная подстановка не является ошибкой компиляции"

```c++
template <typename T>
auto f(const T&) {
  std::cout << 1;
  return 0;
}

auto f(...) {
  std::cout << 2;
  return 0;
}
```

f(5) -> 1 (очевидно)

```c++
template <typename T>
auto f(const T&) -> decltype(T().size()) {
  std::cout << 1;
  return 0;
}

auto f(...) {
  std::cout << 2;
  return 0;
}
```

Если сейчас вызвать f от какого-нибудь вектора, то выведется 1 (причем это будет size_t), а если от инта то выведется 2 причем инт. Собственно тут и происходит SFINAE. При подставновке int в первую версию функции возникает ошибка подстановки (нельзя сделать int().size()), но это не ошибка компиляции и компилятор пытается выбрать следующую перегрузку


Это работает только если ошибка возникает именно во время подстановки в сигнатуру функции, а не в тело 

SFINAE позволяет делать нам кучу интересных вещей, давайте с ними разбираться

### 2. std::enable_if использование и реализация

Очень часто мы хотим выключать какие-то перегрузки в зависимости от типа T. В этом нам поможет enable_if

Пример: хотим включать перегрузку только если T это класс. Воспользуемся std::is_class

```c++
#include <type_traits>

template <typename T, typename = std::enable_if_t<std::is_class_v<std::remove_reference_t<T>>>>
void g(const T&) {
  std::cout << 1;
}

template <typename T, typename = std::enable_if_t<!std::is_class_v<std::remove_reference_t<T>>>>
void g(T&&) {
  std::cout << 2;
}
```

Давайте реализуем

```c++
template <bool B, typename = void>
struct enable_if {};

template <typename T>
struct enable_if<true, T> {
  using type = T;
}

template <bool B, typename T = void>
using enable_if_t = typename enable_if<B,T>::type;
```

### 3. Проверка наличия метода в классе

Давайте напишем мета функцию, которая будет проверять есть ли в классе метод construct с данными аргументами


```c++
template <typename T, typename... Args>
struct has_method_construct {
 public:
  static const bool value = ...;
 private:

}

template <typename T, typename... Args>
const bool has_method_construct_v = has_method_construct<T, Args...>::value
```

Нужно написать какой-то метод в приватной части, который будет с использованием SFINAE все определять

```c++
template <typename T, typename... Args>
struct has_method_construct {
 private:
  static auto f(int) -> decltype(T().construct(Args()...));
  static bool f(...);
 public:
  static const bool value = ...;
}

template <typename T, typename... Args>
const bool has_method_construct_v = has_method_construct<T, Args...>::value
```

Есть проблема: мы хотим чтобы возвращаемое значение было bool в идеале, но у нас возвращаемое значение это decltype(T().construct(Args()...)). На помощь нам приходит оператор запятая

```c++
template <typename T, typename... Args>
struct has_method_construct {
 private:
  static auto f(int) -> decltype(T().construct(Args()...), bool());
  static bool f(...);
 public:
  static const bool value = ...;
}

template <typename T, typename... Args>
const bool has_method_construct_v = has_method_construct<T, Args...>::value
```

Теперь нам очень хочется написать так: static const bool value = f(0), но нам же нужно делать проверки в compile-time

Можно написать что эти функции constexpr и тогда компилятор будет обязан посчитать их на этапе компиляции, но давайте попробуем по-другому. Мы умеем сравнивать типы, давайте все таки сделаем разные типы у наших функций

```c++
template <typename T, typename... Args>
struct has_method_construct {
 private:
  static auto f(int) -> decltype(T().construct(Args()...), int());
  static char f(...);
 public:
  static const bool value = std::is_same_v<decltype(f(0)), int>;
}

template <typename T, typename... Args>
const bool has_method_construct_v = has_method_construct<T, Args...>::value
```

Такая реализация не будет работать, когда метода нет. Почему? Потому что T у нас шаблонный параметр класса, а не функции, поэтому SFINAE не отрабатывает

```c++
template <typename T, typename... Args>
struct has_method_construct {
 private:
  template <typename TT, typename... AArgs>
  static auto f(int) -> decltype(TT().construct(AArgs()...), int());

  template <typename...>
  static char f(...);
 public:
  static const bool value = std::is_same_v<decltype(f<T, Args...>(0)), int>;
}

template <typename T, typename... Args>
const bool has_method_construct_v = has_method_construct<T, Args...>::value
```

Это все еще не совсем верная реализация, потому что у типа T как и у типа Args может не быть конструктора по умолчанию. 

(можно проверить написав класс NoDefaultConstruct и принимать его в качестве аргумента в методе construct())

### 4. Проблема конструктора по умолчанию и declaval()

Для типа T мы хотим иметь выражение которое будет иметь тип T, при этом у типа T может отсутствовать конструктор по умолчанию

Для этого существует функция declval. Первое что приходит в голову:

```c++
template <typename T>
T declval() noexcept;
```

Тогда нашу функцию has_method_construct нужно переписать так:

```c++
template <typename T, typename... Args>
struct has_method_construct {
 private:
  template <typename TT, typename... AArgs>
  static auto f(int) -> decltype(declval<TT>().construct(declval<AArgs>()...), int());

  template <typename...>
  static char f(...);
 public:
  static const bool value = std::is_same_v<decltype(f<T, Args...>(0)), int>;
}

template <typename T, typename... Args>
const bool has_method_construct_v = has_method_construct<T, Args...>::value
```

Что написано в теле функции declval? Ничего. Эта функция нужна только для compile-time приколов. 

А еще эта функция на самом деле возвращает T&&

```c++
template <typename T>
T&& declval() noexcept;
```

Это позволяет нам работать с типами у которых нет тела, а так же с шаблонными типами которые не придется инстанцировать (incomplete type)

### 5. move_if_noexcept

Довольно важная метафункция, она нужна нам чтобы гарантировать безопасность относительно исключений (ведь если исключение происходит во время мува, то это невозможно нормально исправить)

```c++
template<typename Tp>
struct move_if_noexcept_cond
: public and<__not_<is_nothrow_move_constructible<Tp>>,
             is_copy_constructible<Tp>>::type 
{ };

template<typename Tp>
constexpr typename
conditional<move_if_noexcept_cond<std::remove_reference_t<Tp>>::value, const std::remove_reference_t<Tp>&, std::remove_reference_t<Tp>&&>::type
move_if_noexcept(std::remove_reference_t<Tp>& x) noexcept
{ return std::move(x); }
```

Вся задумка в возвращаемом значении: если все хорошо то мы вернем T&&, а если нет то const T&

### 6. Вычисления с помощью шаблонов

#### Вычисление N-го числа Фибоначчи

Давайте попробуем написать вычисление N-го числа Фибоначчи на шаблонах:

```c++
template <size_t N>
struct Fibonacci {
  static const size_t value = Fibonacci<N-1>::value + Fibonacci<N-2>::value;
}

template <>
struct Fibonacci<0> {
  static const size_t value = 0;
}

template <>
struct Fibonacci<1> {
  static const size_t value = 1;
}
```

#### Проверка простоты числа

Иногда нам может быть важно простое ли число нам передали в шаблон (например при написании класса поля). Мы напишем достаточно тупую проверку за линюю. 

Для начала давайте напишем PrimeHelper

```c++
template <size_t N, size_t D>
struct IsPrimeHelper {
  static const bool value = N % D == 0 ? false : IsPrimeHelper<N, D-1>::value;
};

template <size_t N, size_t D>
struct IsPrimeHelper<N, 1> {
  static const bool value = true;
}
```

Теперь можно написать IsPrime

```c++
template <size_t N>
strict IsPrime {
  static const bool value = IsPrimeHelper<N, N-1>::value;
}

template <>
strict IsPrime<1> {
  static const bool value = false;
}

template <>
strict IsPrime<0> {
  static const bool value = false;
}
```

У этой реализации есть проблема: если мы возьмем достаточно большое число, то мы упадем с ошибкой компиляции (превысим глубину шаблонной рекурсии)

Задача на подумать: реализуйте проверку простоты за корень

### 7. constexpr функции и переменные (since c++11)

Иногда нам нужны константы, известные на этапе компиляции. Например при объявлении std::array

Для таких констант существует ключевое слово constexpr. 

```c++
int main() {
  constexpr int x = 10;
  std::array<int, x> a;
}
```

Но constexpr могут быть не только переменные, но и функции:

```c++
constexpr int factorial(int n) {
  if (n == 0) {
    return 1;
  }

  return n * factorial(n-1);
}

int main() {
  constexpr int x = 3;
  std::array<int, factorial(x)> a;
}
```

Также стоит сказать про if constexpr (since c++17)

```c++
if constexpr (sizeof(int) == 4) {
  std::cout << 1;
}
```

Главное отличие if constexpr от обычного if'а: не true ветки не будут компилироваться. 

***Note*** Очевидно, что внутри if constexpr можно писать только constexpr выражения

Вернемся к constexpr функциям. В них, например, нельзя создавать объекты. То есть такой код не скомпилируется:

```c++
struct S {
  int x;
}

constexpr int f(int n) {
  S s;
  return 0;
}

int main() {
  f(0);
}
```

Но если пометить конструктор как constexpr, то скомпилируется:

```c++
struct S {
  constexpr S() {}
  int x;
}

constexpr int f(int n) {
  S s;
  return 0;
}

int main() {
  f(0);
}
```

В constexpr функциях нельзя бросать исключения. 

При этом throw написать можно:

```c++
constexpr int f(int n) {
  if (n == 0) {
    throw 1;
  }

  return n;
}

int main() {
  f(10);
}
```


***Note*** в c++20 можно делать даже new в compile-time, но это уже совсем другая история...


Еще одно замечание: constexpr функции вычисляются на этапе компиляции, только если их вызов происходит в constexpr выражении