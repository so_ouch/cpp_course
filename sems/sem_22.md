# Семинар 22

Этот семинар будет посвящен библиотеке ranges

1. Зачем вообще нужны ренжи?

  1. Есть у нас код алгоритма for_each.
    
  ```c++
    template <typename InputIt, typename UnaryFunc>
    UnaryFunc for_each(InputIt first, InputIt last, UnaryFunc f) {
      for (; first != last; ++first) {
        f(*first);
      }
      return f;
    }
  ``` 
    Кстати InputIt и UnaryFunc могут быть концептами! 

    Но last на самом деле не должен быть input_iterator (обсудите почему. пример: итератор на istream). От last на самом деле требуется чтобы он был неким "ограничителем". Делаем вывод что утверждение "диапазон это пара итераторов" это слишком сильно

    Еще один наброс: зачем возвращаем UnaryFunc (может быть интересен стейт предиката после выполнения, ибо for_each гарантирует порядок).
  2. Давайте вызовем for_each с лямбдой 
  ```c++
    [os](ind d) {if (d < 5) os << d * 2 < " "; };
  ```
    Что-то это не похоже на for_each. Смахивает на transform_copy_if, которого естественно нет в стандартной библиотеке (даже transform_if нет).

    Если попытаться написать свою версию то получается какая-то дичь: нужно два предиката передавать и как-то все это неудобно. Давайте попробуем сначала применить copy_if а затем transform.

    Нам придется завести отдельный промежуточный вектор, куда мы и будем копировать с помощью copy_if. Не круто: лишняя память и делаем за два прохода а не один. 

    Хотим добиться поведения схожего с future (громко сказано. Очень.): чтобы copy_if на самом деле ничего не копировал никуда пока его не попросят, а трансформ не трансформировал.

    Таким образом мы сможем легко комбинировать алгоритмы без лишних промежуточных векторов

  3. Как решается задача с помощью ranges:

  ```c++
  auto v = ranges::istream_view<int>(is) |
         | ranges::views::filter([](int n){return n < 5; })
         | ranges::views::transform([](int n){return n * 2; })
  ranges::copy(v, std::ostream_iterator<int>{os, " "});
  ```

  Внимание: это может не работать на clang-15! Причем сообщения об ошибке будут неприятные) 

  4. std::sort VS ranges::sort: какие ошибки будут при вызове от листа? Какие шаблонные параметры и концепты использует ranges::sort?  Что за концепты ranges::begin и ranges::end? Что такое концепт random_access_range?
  5. Что возвращает ranges::sort? Что такое borrowed_iterator_t (его нельзя разименовать и это прекрасно)
  6. Что такое концепт view? Функция views::all(). views::all(s) VS string_view (string_view стирает инфу о том на какой он тип. То есть string_view от std::string будет string_view на чар)
  7. Примеры использованимя view с алгоритмами из ranges. Пример когда итератор на конец это не итератор: 
  ```c++
  template <int Value>
  struct EndValue {
    template <typename It>
    bool operator==(It it) const {
      return *it == Value;
    }
  }
  std::vector<int> v{1,2,3,4};
  auto w = ranges::subrange(v.begin() + 1, EndValue<8>{});
  auto res = accumulate(w);
  ```
  8. Что такое common_view? (нужно для поддержки легаси алгоритмов). Кстати в примере из прошлого пункта common_view не сработает: нужно добавить штук в EndValue

