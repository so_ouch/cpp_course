# Семинар 21

Этот семинар будет посвящен варианту :):):)

1. Что это такое, как этим пользоваться
2. Основа реализации: заводим VariadicUnion на вариадиках в котором лежит Head и VariadicUnion от tail
3. Как реализовать get по индексу и по типу (надо написать шаблонную метафункцию get_index_by_type)
4. Реализация конструкторов: гениальное наследование 

```cpp
template <typename... Types>
class variant: private VariantAlternative<Types, Types...>... {};
```
Заводим там конструкторы и пользуемся гениальной конструкцией чтобы они проросли в наш юнион: 

```cpp
using VariantAlternative<Types, Types...>::VariantAlternative...;
```

5. Деструктры: делаем в VariadicUnion destroy и делаем destroy в  VariantAlternative. В деструкторе самого варианта пользуемся fold-expression:

```cpp
~variant() {
   (VariantAlternative<Types, Types...>::destroy(), ...);
}
```
(Тут кстати стоит рассказать про fold-expression побольше, потому что этого не было на лекциях и не будет)

Как готовиться: в конспекте 20й лекции будет это все. Так же есть лекция Ильи [для продвы](https://www.youtube.com/watch?v=J75H-x6bamY&list=PL4_hYwCyhAvaWsj3-0gLH_yEZfKdTife0&index=13
) и [для основы](https://www.youtube.com/watch?v=Z-v0Lf0b1DE) 

Хочется чтобы по итогу вы написали работающий вариант без оператора присваивания и чтобы все осознали как это сделать.