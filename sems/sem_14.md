# Семинар 14

1. Адаптеры над контейнерами: stack, queue, priority_queue, как они устроены
2. std::array и его устройство
3. std::bitset
4. std::string_view
5. Написать LRU-cash https://leetcode.com/problems/lru-cache/
