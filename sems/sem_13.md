# Семинар 13 

На лекции говорили про стандартные контейнеры (их ассимптотику и что с ними можно делать). 

Темы для обсуждения на семинаре: 

0. Если не обсуждали темы из 12го семинара, то самое время
1. Часть про исключения: 
    1. Иерархия исключений в STL. Примеры ситуаций, когда уместно то или иное исключение.
    2. Спецификатор и оператор noexcept. Примеры использования. Примеры noexcept и не noexcept функций из stl
    3. Расставить noexcept в каком-нибудь простом классе (vector/string)
    4. Рассказать про exception-safety (строгая и базовая гарантия). Реализовать пример https://gitlab.com/slon/shad-cpp0/-/tree/master/safe-transform для демонстрации строгой гарантии. Лямбды мы еще не проходили, так что используем вместо них указатели на функции (или функторы)
2. Рассказать про юзинги, которые используются в итераторах: value_type, iterator_category и тд (можно посмотреть на cppref)
3. На примерах объяснить разницу между input и forward итераторами. (Идея “однопроходного алгоритма”.) Показать примеры алгоритмов, для которых достаточно input, а для которых нужны именно forward. Примеры задач, на которых можно продемонстрировать идею:
    1. https://leetcode.com/problems/best-time-to-buy-and-sell-stock/ и https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/ каких итераторов достаточно в каждом из случаев?
    2. https://leetcode.com/problems/majority-element/ - каких итераторов тут достаточно? (достаточно лишь input-итераторов, см. последнее решение!)
    3. https://leetcode.com/problems/container-with-most-water/ - можно ли обойтись лишь input-итераторами?
    4. https://leetcode.com/problems/trapping-rain-water/ - можно ли обойтись лишь input-итераторами? (Да, см. последнее решение)
