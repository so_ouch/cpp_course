# Семинар 18

1. Поговорить подробнее про structured bindings и tuple-like types. Заодно познакомить с tuple и его использованием
2. Вдогонку про std::tuple и мув-семантику: про разницу между std::tie, std::make_tuple и std::forward_as_tuple: https://www.fluentcpp.com/2020/10/16/tie-make_tuple-forward_as_tuple-how-to-build-a-tuple-in-cpp/
3. Поговорить про https://en.cppreference.com/w/cpp/language/class_template_argument_deduction (этого не было на лекциях и не будет). 
4. Поговорить про то как пользоваться умными указателями. Показать почему надо пользоваться std::make_unique(shared). 
5. Показать unique_ptr для массивов
6. Показать пример использования unique_ptr (или shared_ptr) с нестандартным Deleter: например, на открытие и закрытие файла
