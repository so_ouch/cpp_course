# Семинар 17

1. Поговорить про std::forward его использование и реализацию. Разобрать пример почему такая реализация forward неправильная https://stackoverflow.com/questions/42329230/difference-between-stdforward-implementation

2. Из книжки Мейерса https://coollib.net/b.usr/Skott_Meyers_Effektivnyiy_i_sovremennyiy_C%2B%2B.pdf разобрать параграф 5.4 8.1 и опционально 5.8

3. Реализовать std::exchange https://en.cppreference.com/w/cpp/utility/exchange

4. Реализовать move_iterator https://en.cppreference.com/w/cpp/iterator/move_iterator